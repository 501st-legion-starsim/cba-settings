// 101st Aux Mod
force DBA_CapitalShips_Barrage_Velocity = 100;
force DBA_CapitalShips_MoveShip_ExternalCamera = true;
force DBA_Common_Debug = false;
force DBA_Hyperspace_Offset_Acclamator = 90;
force DBA_Hyperspace_Offset_Default = 0;
force DBA_Hyperspace_Offset_Munificent = 90;
force DBA_Hyperspace_Offset_Quasar = 270;
force DBA_LAAT_InteriorLight_PositionY = -5;
force DBA_LAAT_InteriorLight_PositionZ = 2;
force DBA_Stims_Adrenal_Duration = 30;
force DBA_Stims_Adrenal_Side_Effect_Duration = 60;
force DBA_Stims_Adrenal_Side_Effect_Speed = 0.75;
force DBA_Stims_Adrenal_Speed = 1.25;
force DBA_Stims_Battle_Stim_Duration = 120;
force DBA_Stims_Battle_Stim_Side_Effect_Duration = 60;
force DBA_Stims_Battle_Stim_Side_Effect_Pain_Multiplier = 2;
force DBA_Stims_Inject_Duration = 5;

// 3AS
force TAS_fullShieldTime = 30;
force TAS_jetcoolset = 1;
force TAS_jetfuelset = 1;
force TAS_jetheatset = 1;
force TAS_jetsoundvol = 0.6;
force TAS_lcLoadDist = 20;
force TAS_lcLoadSpeed = 10;
force TAS_shieldTime = 30;
force TAS_SquadShieldTime = 240;

// 3AS Shields
force TAS_DroidekaDisabledShieldTime = 1;
force TAS_DroidekaShieldsRegenDisabled = true;

// ACE Advanced Ballistics
force ace_advanced_ballistics_ammoTemperatureEnabled = true;
force ace_advanced_ballistics_barrelLengthInfluenceEnabled = true;
force ace_advanced_ballistics_bulletTraceEnabled = true;
force ace_advanced_ballistics_enabled = false;
force ace_advanced_ballistics_muzzleVelocityVariationEnabled = true;
force ace_advanced_ballistics_simulationInterval = 0.05;

// ACE Advanced Throwing
ace_advanced_throwing_enabled = true;
force ace_advanced_throwing_enablePickUp = true;
force ace_advanced_throwing_enablePickUpAttached = true;
force ace_advanced_throwing_enableTempWindInfo = true;
ace_advanced_throwing_showMouseControls = true;
ace_advanced_throwing_showThrowArc = true;

// ACE Advanced Vehicle Damage
force ace_vehicle_damage_enableCarDamage = false;
force force ace_vehicle_damage_enabled = false;

// ACE AI
force ace_ai_assignNVG = false;

// ACE Arsenal
force ace_arsenal_allowDefaultLoadouts = true;
force ace_arsenal_allowSharedLoadouts = true;
ace_arsenal_camInverted = false;
ace_arsenal_defaultToFavorites = false;
force ace_arsenal_enableIdentityTabs = true;
ace_arsenal_enableModIcons = true;
ace_arsenal_EnableRPTLog = false;
ace_arsenal_favoritesColor = [0.9,0.875,0.6];
ace_arsenal_fontHeight = 4.5;
ace_arsenal_loadoutsSaveFace = false;
ace_arsenal_loadoutsSaveInsignia = true;
ace_arsenal_loadoutsSaveVoice = false;

// ACE Artillery
force ace_artillerytables_advancedCorrections = false;
force ace_artillerytables_disableArtilleryComputer = false;
force ace_mk6mortar_airResistanceEnabled = false;
force ace_mk6mortar_allowCompass = true;
force ace_mk6mortar_allowComputerRangefinder = true;
force ace_mk6mortar_useAmmoHandling = false;

// ACE Captives
force ace_captives_allowHandcuffOwnSide = true;
force ace_captives_allowSurrender = true;
force ace_captives_requireSurrender = 0;
force ace_captives_requireSurrenderAi = false;

// ACE Casings
ace_casings_enabled = true;
ace_casings_maxCasings = 250;

// ACE Common
force ace_common_allowFadeMusic = true;
force force ace_common_checkPBOsAction = 2;
force force ace_common_checkPBOsCheckAll = true;
force force ace_common_checkPBOsWhitelist = "[""3denEnhanced"",""CHTR_TFAR_Setter"",""MIRA_Vehicle_Medical"",""UPSL_aime"",""UPSL_aime_uav_terminal"",""UPSL_aime_vehicle_controls"",""UPSL_aime_vehicle_seats"",""UPSL_aime_change_ammo"",""UPSL_aime_group"",""UPSL_aime_inventory"",""athena"",""DNI_ZeusFPSMonitor"",""Radio_Animations"",""GF_ReColor"",""DIS_Enhanced_Gps"",""DIS_enhanced_map_ace"",""A3TI"",""3rdView"",""WP_Mod"",""CHVD"",""viewDistance_TAW"",""DS_OBE"",""DS_OBE_Layer"",""Vile_HUD"",""L_Suppress_Suppress_main"",""UTGX_Compass"",""ReducedHazeMod"",""real_fix_3rd_person_view"",""dwyl_main"",""rwyl_main"",""ZEI"",""DEVAS_TacticalReady"",""PH_TacReady"",""kka3_gestures"",""kka3_gestures_ace"",""UtesLowGrass"",""LowGrassChernarus"",""LowGrassChernarus_Summer"",""mrb_a3_seavesselvisibility"",""LowGrassBootcamp_acr"",""VANA"",""mrb_a3_vehiclevisibility"",""mrb_a3_airvisibility"",""ANZ_NoGrassMod"",""gcam"",""CAU_UserInputMenus"",""CAU_colorPicker"",""CW_9LINERS_AND_NOTEPAD"",""bettinv_main"",""bettinv_main_ace"",""ASAAYU_ACE_MEDICAL_ASSISTANT"",""KS_AVD"",""crowsza_main"",""crowsza_misc"",""crowsza_pingbox"",""crowsza_teleport"",""crowsza_tfar"",""crowsza_zeus_text"",""crowsza_ace"",""crowsza_drawbuild"",""cba_settings_userconfig"",""sez_main"",""uav_turret_markers"",""tfar_ace_extended_main""]";
force force ace_common_deployedSwayFactor = 0.3;
ace_common_displayTextColor = [0,0,0,0.1];
ace_common_displayTextFontColor = [1,1,1,1];
force force ace_common_enableSway = true;
ace_common_epilepsyFriendlyMode = false;
ace_common_progressBarInfo = 2;
force force ace_common_restedSwayFactor = 0.3;
ace_common_settingFeedbackIcons = 1;
ace_common_settingProgressBarLocation = 0;
force force ace_common_swayFactor = 0.3;

// ACE Cook-off
force ace_cookoff_ammoCookoffDuration = 1;
force ace_cookoff_cookoffDuration = 1;
force ace_cookoff_destroyVehicleAfterCookoff = false;
force ace_cookoff_enableAmmobox = false;
force ace_cookoff_enableAmmoCookoff = false;
force ace_cookoff_enableFire = false;
force ace_cookoff_probabilityCoef = 1;
force ace_cookoff_removeAmmoDuringCookoff = true;

// ACE Crew Served Weapons
force ace_csw_ammoHandling = 2;
force ace_csw_defaultAssemblyMode = false;
ace_csw_dragAfterDeploy = false;
force ace_csw_handleExtraMagazines = true;
force ace_csw_handleExtraMagazinesType = 0;
force ace_csw_progressBarTimeCoefficent = 1;

// ACE Dragging
force force ace_dragging_allowRunWithLightweight = true;
force force ace_dragging_dragAndFire = true;
force force ace_dragging_skipContainerWeight = true;
force force ace_dragging_weightCoefficient = 0;

// ACE Explosives
ace_explosives_customTimerDefault = 30;
force ace_explosives_customTimerMax = 900;
force ace_explosives_customTimerMin = 5;
force ace_explosives_explodeOnDefuse = false;
force ace_explosives_punishNonSpecialists = false;
force ace_explosives_requireSpecialist = false;

// ACE Field Rations
force acex_field_rations_affectAdvancedFatigue = true;
force force acex_field_rations_enabled = false;
acex_field_rations_hudShowLevel = 0;
acex_field_rations_hudTransparency = -1;
acex_field_rations_hudType = 0;
force acex_field_rations_hungerSatiated = 1;
force acex_field_rations_terrainObjectActions = true;
force acex_field_rations_thirstQuenched = 1;
force acex_field_rations_timeWithoutFood = 2;
force acex_field_rations_timeWithoutWater = 2;
force acex_field_rations_waterSourceActions = 2;

// ACE Fire
force ace_fire_dropWeapon = 1;
force force ace_fire_enabled = false;
force ace_fire_enableFlare = false;
ace_fire_enableScreams = true;

// ACE Fortify
force ace_fortify_markObjectsOnMap = 1;
force force ace_fortify_timeCostCoefficient = 1;
force force ace_fortify_timeMin = 1.5;
force force acex_fortify_settingHint = 1;

// ACE Fragmentation Simulation
force force ace_frag_enabled = false;
force force ace_frag_reflectionsEnabled = false;
force force ace_frag_spallEnabled = false;
force ace_frag_spallIntensity = 1;

// ACE G-Forces
force force ace_gforces_coef = 1;
force force ace_gforces_enabledFor = 0;

// ACE Goggles
ace_goggles_effects = 2;
ace_goggles_showClearGlasses = false;
ace_goggles_showInThirdPerson = false;

// ACE Grenades
force ace_grenades_convertExplosives = true;

// ACE Headless
force acex_headless_delay = 15;
force force acex_headless_enabled = false;
force acex_headless_endMission = 0;
force acex_headless_log = false;
force acex_headless_transferLoadout = 0;

// ACE Hearing
force ace_hearing_autoAddEarplugsToUnits = 1;
ace_hearing_disableEarRinging = true;
force ace_hearing_earplugsVolume = 0.5;
force force ace_hearing_enableCombatDeafness = false;
force ace_hearing_enabledForZeusUnits = true;
force ace_hearing_explosionDeafnessCoefficient = 1;
force ace_hearing_unconsciousnessVolume = 0.647813;

// ACE Interaction
force force ace_interaction_disableNegativeRating = false;
force ace_interaction_enableAnimActions = true;
force force ace_interaction_enableGroupRenaming = true;
ace_interaction_enableMagazinePassing = false;
force force ace_interaction_enableTeamManagement = true;
ace_interaction_enableWeaponAttachments = false;
force ace_interaction_interactWithEnemyCrew = 0;
force force ace_interaction_interactWithTerrainObjects = false;
force ace_interaction_remoteTeamManagement = true;

// ACE Interaction Menu
ace_gestures_showOnInteractionMenu = 2;
ace_interact_menu_actionOnKeyRelease = true;
ace_interact_menu_addBuildingActions = false;
ace_interact_menu_alwaysUseCursorInteraction = false;
ace_interact_menu_alwaysUseCursorSelfInteraction = true;
ace_interact_menu_colorShadowMax = [0,0,0,1];
ace_interact_menu_colorShadowMin = [0,0,0,0.25];
ace_interact_menu_colorTextMax = [1,1,1,1];
ace_interact_menu_colorTextMin = [1,1,1,0.25];
ace_interact_menu_consolidateSingleChild = false;
ace_interact_menu_cursorKeepCentered = false;
ace_interact_menu_cursorKeepCenteredSelfInteraction = false;
ace_interact_menu_menuAnimationSpeed = 0;
ace_interact_menu_menuBackground = 0;
ace_interact_menu_menuBackgroundSelf = 0;
ace_interact_menu_selectorColor = [1,0,0];
ace_interact_menu_shadowSetting = 2;
ace_interact_menu_textSize = 2;
ace_interact_menu_useListMenu = false;
ace_interact_menu_useListMenuSelf = true;

// ACE Interaction Menu (Self) - More
ace_interact_menu_more__ACE_CheckAirTemperature = false;
ace_interact_menu_more__ace_csw_deploy = false;
ace_interact_menu_more__ACE_Equipment = false;
ace_interact_menu_more__ACE_Explosives = false;
ace_interact_menu_more__ace_field_rations = false;
ace_interact_menu_more__ace_fortify = false;
ace_interact_menu_more__ace_gestures = false;
ace_interact_menu_more__ace_intelitems = false;
ace_interact_menu_more__ACE_MapFlashlight = false;
ace_interact_menu_more__ACE_MapGpsHide = false;
ace_interact_menu_more__ACE_MapGpsShow = false;
ace_interact_menu_more__ACE_MapTools = false;
ace_interact_menu_more__ACE_Medical = false;
ace_interact_menu_more__ACE_Medical_Menu = false;
ace_interact_menu_more__ACE_MoveRallypoint = false;
ace_interact_menu_more__ACE_PlottingBoard = false;
ace_interact_menu_more__ACE_PlottingBoardHide = false;
ace_interact_menu_more__ACE_RepackMagazines = false;
ace_interact_menu_more__ace_sandbag_place = false;
ace_interact_menu_more__ACE_StartSurrenderingSelf = false;
ace_interact_menu_more__ACE_StopEscortingSelf = false;
ace_interact_menu_more__ACE_StopSurrenderingSelf = false;
ace_interact_menu_more__ACE_Tags = false;
ace_interact_menu_more__ACE_TeamManagement = false;
ace_interact_menu_more__ace_zeus_create = false;
ace_interact_menu_more__ace_zeus_delete = false;
ace_interact_menu_more__aceax_ingame_gear = false;
ace_interact_menu_more__acex_sitting_Stand = false;
ace_interact_menu_more__DBA_Stims = false;
ace_interact_menu_more__IBL_weaponStates = false;
ace_interact_menu_more__JLTS_repairWeapons = false;
ace_interact_menu_more__KAT_Equipment = false;
ace_interact_menu_more__kka3_anim = false;
ace_interact_menu_more__Medical = false;
ace_interact_menu_more__RD501 Lights = false;
ace_interact_menu_more__TFAR_Radio = false;
ace_interact_menu_more__Zeus = false;

// ACE Interaction Menu (Self) - Move to Root
ace_interact_menu_moveToRoot__ACE_Equipment__ace_atragmx_open = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_attach_Attach = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_attach_Detach = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ACE_CheckDogtags = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ACE_Chemlights = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_dagr_menu = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_dagr_menu__ace_dagr_toggle = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_goggles_wipeGlasses = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_gunbag_actions = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_gunbag_actions__ace_gunbag_status = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_gunbag_actions__ace_gunbag_weaponOff = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_gunbag_actions__ace_gunbag_weaponSwap = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_gunbag_actions__ace_gunbag_weaponTo = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_huntir_open = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_interaction_weaponAttachments = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_kestrel4500_open = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_kestrel4500_open__ace_kestrel4500_hide = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_kestrel4500_open__ace_kestrel4500_show = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_marker_flags = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_microdagr_configure = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_microdagr_configure__ace_microdagr_close = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_microdagr_configure__ace_microdagr_show = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_minedetector_metalDetector = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_minedetector_metalDetector__ace_minedetector_activate = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_minedetector_metalDetector__ace_minedetector_connectHeadphones = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_minedetector_metalDetector__ace_minedetector_deactivate = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_minedetector_metalDetector__ace_minedetector_disconnectHeadphones = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_mk6mortar_rangetable = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_overheating_CheckTemperature = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_overheating_CheckTemperatureSpareBarrels = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_overheating_CoolWeaponWithItem = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_overheating_SwapBarrel = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_overheating_UnJam = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ACE_PutInEarplugs = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_rangecard_open = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_rangecard_open__ace_rangecard_makeCopy = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_rangecard_open__ace_rangecard_openCopy = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_reload_checkAmmo = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ACE_RemoveEarplugs = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_scopes_adjustZero = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_scopes_resetZero = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_spottingscope_place = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ACE_TacticalLadders = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_trenches = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_trenches__ace_trenches_digEnvelopeBig = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_trenches__ace_trenches_digEnvelopeSmall = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_trenches__grad_trenches_digEnvelopeGiant = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_trenches__grad_trenches_digEnvelopeLongNameEmplacment = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_trenches__grad_trenches_digEnvelopeShort = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_trenches__grad_trenches_digEnvelopeVehicle = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ace_tripod_place = false;
ace_interact_menu_moveToRoot__ACE_Equipment__AMP_DeployDoorWedge = false;
ace_interact_menu_moveToRoot__ACE_Equipment__kka3_placeCampfireburningF = false;
ace_interact_menu_moveToRoot__ACE_Equipment__kka3_placeLandBagFenceLong = false;
ace_interact_menu_moveToRoot__ACE_Equipment__kka3_placeLandBagFenceRound = false;
ace_interact_menu_moveToRoot__ACE_Equipment__kka3_placeLandCampingChairV1F = false;
ace_interact_menu_moveToRoot__ACE_Equipment__kka3_placeLandCampingLightoffF = false;
ace_interact_menu_moveToRoot__ACE_Equipment__kka3_placeLandCampingTableF = false;
ace_interact_menu_moveToRoot__ACE_Equipment__kka3_placeLandPalletvertical = false;
ace_interact_menu_moveToRoot__ACE_Equipment__kka3_placeLandPortableLightsingle = false;
ace_interact_menu_moveToRoot__ACE_Equipment__kka3_placeRoadBarrier = false;
ace_interact_menu_moveToRoot__ACE_Equipment__kka3_placeRoadBarriersmall = false;
ace_interact_menu_moveToRoot__ACE_Equipment__kka3_placeRoadCone = false;
ace_interact_menu_moveToRoot__ACE_Equipment__kka3_placeRoadConeLight = false;
ace_interact_menu_moveToRoot__ACE_Equipment__kka3_placeTapeSignF = false;
ace_interact_menu_moveToRoot__ACE_Equipment__kka3_placeTargetF = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ls_holomap_deploy = false;
ace_interact_menu_moveToRoot__ACE_Equipment__ls_meme_eatChocolate = false;
ace_interact_menu_moveToRoot__ACE_Equipment__MRH_Ace_SATCOM_ANTENNA = false;
ace_interact_menu_moveToRoot__ACE_Equipment__MRH_Ace_USE_UTD = false;
ace_interact_menu_moveToRoot__ACE_Equipment__RD501_Auto_Radio_Freq = false;
ace_interact_menu_moveToRoot__ACE_Equipment__slingHelmet = false;
ace_interact_menu_moveToRoot__ACE_Equipment__slingHelmet__slingLeft = false;
ace_interact_menu_moveToRoot__ACE_Equipment__slingHelmet__slingRight = false;
ace_interact_menu_moveToRoot__ACE_Equipment__unslingHelmet = false;
ace_interact_menu_moveToRoot__ACE_Equipment__zade_boc_onBack = false;
ace_interact_menu_moveToRoot__ACE_Equipment__zade_boc_onChest = false;
ace_interact_menu_moveToRoot__ACE_Equipment__zade_boc_swap = false;
ace_interact_menu_moveToRoot__ACE_Explosives__ACE_Cellphone = false;
ace_interact_menu_moveToRoot__ACE_Explosives__ACE_Place = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Advance = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_CeaseFire = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Cover = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Engage = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Follow = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Forward = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Freeze = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Go = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Hold = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Point = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Regroup = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Stop = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Up = false;
ace_interact_menu_moveToRoot__ace_gestures__ace_gestures_Warning = false;
ace_interact_menu_moveToRoot__ACE_MapTools__ACE_MapToolsAlign = false;
ace_interact_menu_moveToRoot__ACE_MapTools__ACE_MapToolsAlign__ACE_MapToolsAlignCompass = false;
ace_interact_menu_moveToRoot__ACE_MapTools__ACE_MapToolsAlign__ACE_MapToolsAlignNorth = false;
ace_interact_menu_moveToRoot__ACE_MapTools__ACE_MapToolsAlign__ACE_MapToolsAlignToPlottingBoard = false;
ace_interact_menu_moveToRoot__ACE_MapTools__ACE_MapToolsAlign__ACE_MapToolsAlignToPlottingBoardAcrylic = false;
ace_interact_menu_moveToRoot__ACE_MapTools__ACE_MapToolsAlign__ACE_MapToolsAlignToPlottingBoardRuler = false;
ace_interact_menu_moveToRoot__ACE_MapTools__ACE_MapToolsHide = false;
ace_interact_menu_moveToRoot__ACE_MapTools__ACE_MapToolsShowNormal = false;
ace_interact_menu_moveToRoot__ACE_MapTools__ACE_MapToolsShowSmall = false;
ace_interact_menu_moveToRoot__ACE_Medical__ACE_ArmLeft = false;
ace_interact_menu_moveToRoot__ACE_Medical__ACE_ArmRight = false;
ace_interact_menu_moveToRoot__ACE_Medical__ACE_Head = false;
ace_interact_menu_moveToRoot__ACE_Medical__ACE_LegLeft = false;
ace_interact_menu_moveToRoot__ACE_Medical__ACE_LegRight = false;
ace_interact_menu_moveToRoot__ACE_Medical__ACE_Torso = false;
ace_interact_menu_moveToRoot__ACE_Medical__ACE_Torso__TriageCard = false;
ace_interact_menu_moveToRoot__ACE_Medical__dev_enzymeCapsule = false;
ace_interact_menu_moveToRoot__ACE_Medical__dev_enzymeCapsule_refined = false;
ace_interact_menu_moveToRoot__ACE_PlottingBoardHide__ACE_PlottingBoardAlign = false;
ace_interact_menu_moveToRoot__ACE_PlottingBoardHide__ACE_PlottingBoardAlign__ACE_PlottingBoardAlignAcrylic = false;
ace_interact_menu_moveToRoot__ACE_PlottingBoardHide__ACE_PlottingBoardAlign__ACE_PlottingBoardAlignAcrylic__ACE_PlottingBoardAlignAcrylicMaptool = false;
ace_interact_menu_moveToRoot__ACE_PlottingBoardHide__ACE_PlottingBoardAlign__ACE_PlottingBoardAlignAcrylic__ACE_PlottingBoardAlignAcrylicUp = false;
ace_interact_menu_moveToRoot__ACE_PlottingBoardHide__ACE_PlottingBoardAlign__ACE_PlottingBoardAlignBoard = false;
ace_interact_menu_moveToRoot__ACE_PlottingBoardHide__ACE_PlottingBoardAlign__ACE_PlottingBoardAlignBoard__ACE_PlottingBoardAlignBoardMaptool = false;
ace_interact_menu_moveToRoot__ACE_PlottingBoardHide__ACE_PlottingBoardAlign__ACE_PlottingBoardAlignBoard__ACE_PlottingBoardAlignBoardUp = false;
ace_interact_menu_moveToRoot__ACE_PlottingBoardHide__ACE_PlottingBoardAlign__ACE_PlottingBoardAlignRuler = false;
ace_interact_menu_moveToRoot__ACE_PlottingBoardHide__ACE_PlottingBoardAlign__ACE_PlottingBoardAlignRuler__ACE_PlottingBoardAlignRulerMaptool = false;
ace_interact_menu_moveToRoot__ACE_PlottingBoardHide__ACE_PlottingBoardAlign__ACE_PlottingBoardAlignRuler__ACE_PlottingBoardAlignRulerUp = false;
ace_interact_menu_moveToRoot__ACE_PlottingBoardHide__ACE_PlottingBoardRulerHide = false;
ace_interact_menu_moveToRoot__ACE_PlottingBoardHide__ACE_PlottingBoardRulerShow = false;
ace_interact_menu_moveToRoot__ACE_PlottingBoardHide__ACE_PlottingBoardWipe = false;
ace_interact_menu_moveToRoot__ACE_TeamManagement__ACE_BecomeLeader = false;
ace_interact_menu_moveToRoot__ACE_TeamManagement__ACE_JoinTeamBlue = false;
ace_interact_menu_moveToRoot__ACE_TeamManagement__ACE_JoinTeamGreen = false;
ace_interact_menu_moveToRoot__ACE_TeamManagement__ACE_JoinTeamMain = false;
ace_interact_menu_moveToRoot__ACE_TeamManagement__ACE_JoinTeamRed = false;
ace_interact_menu_moveToRoot__ACE_TeamManagement__ACE_JoinTeamYellow = false;
ace_interact_menu_moveToRoot__ACE_TeamManagement__ACE_LeaveGroup = false;
ace_interact_menu_moveToRoot__ACE_TeamManagement__ACE_remoteTeamManagement = false;
ace_interact_menu_moveToRoot__ACE_TeamManagement__ACE_RenameGroup = false;
ace_interact_menu_moveToRoot__ace_trenches__ace_trenches_digEnvelopeBig = false;
ace_interact_menu_moveToRoot__ace_trenches__ace_trenches_digEnvelopeSmall = false;
ace_interact_menu_moveToRoot__ace_trenches__grad_trenches_digEnvelopeGiant = false;
ace_interact_menu_moveToRoot__ace_trenches__grad_trenches_digEnvelopeLongNameEmplacment = false;
ace_interact_menu_moveToRoot__ace_trenches__grad_trenches_digEnvelopeShort = false;
ace_interact_menu_moveToRoot__ace_trenches__grad_trenches_digEnvelopeVehicle = false;
ace_interact_menu_moveToRoot__DBA_Stims__DBA_Stims_Adrenal = false;
ace_interact_menu_moveToRoot__DBA_Stims__DBA_Stims_Battle = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__LeftArm = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__LeftArm__Doctor = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__LeftArm__Doctor_NVG = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__LeftArm__Kat = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__LeftArm__Kat_NVG = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__LeftArm__Medic = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__LeftArm__Medic_NVG = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__LeftArm__RedCross = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__LeftArm__RedCross_NVG = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__LeftLeg = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__LeftLeg__Doctor = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__LeftLeg__Doctor_NVG = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__LeftLeg__Kat = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__LeftLeg__Kat_NVG = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__LeftLeg__Medic = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__LeftLeg__Medic_NVG = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__LeftLeg__RedCross = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__LeftLeg__RedCross_NVG = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__RightArm = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__RightArm__Doctor = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__RightArm__Doctor_NVG = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__RightArm__Kat = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__RightArm__Kat_NVG = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__RightArm__Medic = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__RightArm__Medic_NVG = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__RightArm__RedCross = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__RightArm__RedCross_NVG = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__RightLeg = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__RightLeg__Doctor = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__RightLeg__Doctor_NVG = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__RightLeg__Kat = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__RightLeg__Kat_NVG = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__RightLeg__Medic = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__RightLeg__Medic_NVG = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__RightLeg__RedCross = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__RightLeg__RedCross_NVG = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__UnSlingLeftArm = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__UnSlingLeftLeg = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__UnSlingRightArm = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Armband__UnSlingRightLeg = false;
ace_interact_menu_moveToRoot__KAT_Equipment__BubbleWrapPopping = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_AED_X_Interactions = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_AED_X_Interactions__KAT_AED_X_addSound = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_AED_X_Interactions__KAT_AED_X_removeSound = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_AED_X_Interactions__KAT_AED_X_ViewMonitor = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_AFAK_Item = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_AFAK_Item__AFAKInfo = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_AFAK_Item__Slot1 = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_AFAK_Item__Slot2 = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_AFAK_Item__Slot3 = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_AFAK_Item__Slot4 = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_AFAK_Item__Slot5 = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_AFAK_Item__Slot6 = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_AFAK_Mag = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_AFAK_Mag__AFAKInfo = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_AFAK_Mag__Slot1 = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_AFAK_Mag__Slot1_Repack = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_AFAK_Mag__Slot2 = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_AFAK_Mag__Slot2_Repack = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_AFAK_Mag__Slot3 = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_AFAK_Mag__Slot3_Repack = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_AFAK_Mag__Slot4 = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_AFAK_Mag__Slot4_Repack = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_AFAK_Mag__Slot5 = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_AFAK_Mag__Slot5_Repack = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_AFAK_Mag__Slot6 = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_AFAK_Mag__Slot6_Repack = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_ChangeGasMaskFilter = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_CheckGasMaskDur = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_CheckHandWarmersSelf = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_DeployStretcher = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_IFAK_Item = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_IFAK_Item__IFAKInfo = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_IFAK_Item__Slot1 = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_IFAK_Item__Slot2 = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_IFAK_Item__Slot3 = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_IFAK_Item__Slot4 = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_IFAK_Mag = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_IFAK_Mag__IFAKInfo = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_IFAK_Mag__Slot1 = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_IFAK_Mag__Slot1_Repack = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_IFAK_Mag__Slot2 = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_IFAK_Mag__Slot2_Repack = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_IFAK_Mag__Slot3 = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_IFAK_Mag__Slot3_Repack = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_IFAK_Mag__Slot4 = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_IFAK_Mag__Slot4_Repack = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_MFAK_Item = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_MFAK_Item__MFAKInfo = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_MFAK_Item__Slot1 = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_MFAK_Item__Slot2 = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_MFAK_Item__Slot3 = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_MFAK_Item__Slot4 = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_MFAK_Item__Slot5 = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_MFAK_Item__Slot6 = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_MFAK_Item__Slot7 = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_MFAK_Item__Slot8 = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_MFAK_Mag = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_MFAK_Mag__MFAKInfo = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_MFAK_Mag__Slot1 = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_MFAK_Mag__Slot1_Repack = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_MFAK_Mag__Slot2 = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_MFAK_Mag__Slot2_Repack = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_MFAK_Mag__Slot3 = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_MFAK_Mag__Slot3_Repack = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_MFAK_Mag__Slot4 = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_MFAK_Mag__Slot4_Repack = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_MFAK_Mag__Slot5 = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_MFAK_Mag__Slot5_Repack = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_MFAK_Mag__Slot6 = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_MFAK_Mag__Slot6_Repack = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_MFAK_Mag__Slot7 = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_MFAK_Mag__Slot7_Repack = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_MFAK_Mag__Slot8 = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_MFAK_Mag__Slot8_Repack = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_MuteChemDetector = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_placeAED = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_placeAEDX = false;
ace_interact_menu_moveToRoot__KAT_Equipment__KAT_UnmuteChemDetector = false;
ace_interact_menu_moveToRoot__KAT_Equipment__openCrossPanel = false;
ace_interact_menu_moveToRoot__KAT_Equipment__PulseOximeter_addSound = false;
ace_interact_menu_moveToRoot__KAT_Equipment__PulseOximeter_removeSound = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Refill_OxygenTank_150_Facility = false;
ace_interact_menu_moveToRoot__KAT_Equipment__Refill_OxygenTank_300_Facility = false;
ace_interact_menu_moveToRoot__KAT_Equipment__SetPreferred_OxygenTank_150 = false;
ace_interact_menu_moveToRoot__KAT_Equipment__SetPreferred_OxygenTank_300 = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_actions = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_actions__kka3_direonepeace = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_actions__kka3_direonerock = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_actions__kka3_Facepalm = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_actions__kka3_kata = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_actions__kka3_pissing = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_actions__kka3_Pushup = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_actions__kka3_Threaten = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_actions__kka3_Thumbsup = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_cancel = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_carrier = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_carrier__kka3_crouch = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_carrier__kka3_crouch_thumbup = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_carrier__kka3_EmergencyStop = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_carrier__kka3_EnginesOff = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_carrier__kka3_EnginesOn = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_carrier__kka3_Launch = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_carrier__kka3_Left = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_carrier__kka3_Right = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_carrier__kka3_Slow = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_carrier__kka3_Stop = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_carrier__kka3_Straight = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_dance = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_dance__kka3_Crazy_Dance = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_dance__kka3_crazydrunkdance = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_dance__kka3_dubstepdance = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_dance__kka3_dubstepPop = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_dance__kka3_DuoIvan = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_dance__kka3_hiphopdance = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_dance__kka3_nightclubdance = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_dance__kka3_robotdance = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_dance__kka3_russiandance = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_direone = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_direone2 = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_direone2__kka3_direone1b = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_direone2__kka3_direone2b = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_direone2__kka3_direone3b = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_direone__kka3_direone_1 = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_direone__kka3_direone_2 = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_direone__kka3_direone_3 = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_direone__kka3_direone_4 = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_direone__kka3_direone_5 = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_direone__kka3_direone_6 = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_direone__kka3_direone_7 = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_emotion = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_emotion__kka3_mimic = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_emotion__kka3_mimic__kka3_Angry = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_emotion__kka3_mimic__kka3_Default = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_emotion__kka3_mimic__kka3_Sad = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_emotion__kka3_mimic__kka3_Smile = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_emotion__kka3_scared = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_poses = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_poses__kka3_pose1 = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_poses__kka3_pose10 = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_poses__kka3_pose2 = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_poses__kka3_pose3 = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_poses__kka3_pose4 = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_poses__kka3_pose5 = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_poses__kka3_pose6 = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_poses__kka3_pose7 = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_poses__kka3_pose8 = false;
ace_interact_menu_moveToRoot__kka3_anim__kka3_poses__kka3_pose9 = false;
ace_interact_menu_moveToRoot__Medical__ACE_Head = false;
ace_interact_menu_moveToRoot__Medical__ACE_Head__CheckBloodPressure = false;

// ACE Kill Tracker
force ace_killtracker_showCrewKills = false;
force force ace_killtracker_trackAI = true;

// ACE Logistics
force force ace_cargo_carryAfterUnload = true;
force force ace_cargo_enable = true;
force force ace_cargo_enableDeploy = true;
force force ace_cargo_enableRename = false;
force force ace_cargo_loadTimeCoefficient = 5;
ace_cargo_openAfterUnload = 0;
force force ace_cargo_paradropTimeCoefficent = 2.5;
force force ace_rearm_distance = 30;
force force ace_rearm_enabled = true;
force force ace_rearm_level = 1;
force force ace_rearm_supply = 0;
force force ace_refuel_cargoRate = 10;
force ace_refuel_enabled = true;
force force ace_refuel_hoseLength = 30;
force force ace_refuel_progressDuration = 2;
force force ace_refuel_rate = 3;
force force ace_towing_addRopeToVehicleInventory = false;

// ACE Magazine Repack
ace_magazinerepack_repackAnimation = true;
ace_magazinerepack_repackLoadedMagazines = true;
force force ace_magazinerepack_timePerAmmo = 1.5;
force force ace_magazinerepack_timePerBeltLink = 8;
force force ace_magazinerepack_timePerMagazine = 2;

// ACE Map
force force ace_map_BFT_Enabled = false;
force ace_map_BFT_HideAiGroups = false;
force ace_map_BFT_Interval = 1;
force ace_map_BFT_ShowPlayerNames = false;
force force ace_map_DefaultChannel = 0;
force force ace_map_mapGlow = true;
force force ace_map_mapIllumination = true;
force force ace_map_mapLimitZoom = false;
force force ace_map_mapShake = true;
force force ace_map_mapShowCursorCoordinates = false;
force force ace_markers_moveRestriction = 0;
force force ace_markers_timestampEnabled = true;
force force ace_markers_timestampFormat = "HH:MM";
force force ace_markers_timestampHourFormat = 24;
force ace_markers_timestampTimezone = 0;
ace_markers_TimestampUTCMinutesOffset = 0;
force ace_markers_timestampUTCOffset = 0;

// ACE Map Gestures
ace_map_gestures_allowCurator = true;
ace_map_gestures_allowSpectator = true;
ace_map_gestures_briefingMode = 0;
ace_map_gestures_defaultColor = [1,0.88,0,0.7];
ace_map_gestures_defaultLeadColor = [1,0.88,0,0.95];
force ace_map_gestures_enabled = true;
force ace_map_gestures_interval = 0.03;
force ace_map_gestures_maxRange = 7;
force ace_map_gestures_maxRangeCamera = 14;
ace_map_gestures_nameTextColor = [0.2,0.2,0.2,0.3];
force ace_map_gestures_onlyShowFriendlys = false;

// ACE Map Tools
ace_maptools_drawStraightLines = true;
ace_maptools_plottingBoardAllowChannelDrawing = 1;
ace_maptools_rotateModifierKey = 1;

// ACE Medical
force force ace_medical_ai_enabledFor = 2;
force force ace_medical_ai_requireItems = 0;
force force ace_medical_AIDamageThreshold = 1;
force force ace_medical_bleedingCoefficient = 0.4;
force force ace_medical_blood_bloodLifetime = 300;
force force ace_medical_blood_enabledFor = 2;
force force ace_medical_blood_maxBloodObjects = 500;
force force ace_medical_deathChance = 0.8;
force force ace_medical_dropWeaponUnconsciousChance = 0;
force force ace_medical_enableVehicleCrashes = false;
force force ace_medical_engine_damagePassThroughEffect = 0;
force force ace_medical_fatalDamageSource = 1;
force force ace_medical_fractureChance = 0.35;
force force ace_medical_fractures = 2;
force force ace_medical_ivFlowRate = 2;
force force ace_medical_limping = 0;
force force ace_medical_painCoefficient = 0.7;
force force ace_medical_painUnconsciousChance = 0.4;
force force ace_medical_painUnconsciousThreshold = 0.5;
force force ace_medical_playerDamageThreshold = 12;
force force ace_medical_spontaneousWakeUpChance = 0.751836;
force force ace_medical_spontaneousWakeUpEpinephrineBoost = 20;
force force ace_medical_statemachine_AIUnconsciousness = false;
force force ace_medical_statemachine_cardiacArrestBleedoutEnabled = true;
force force ace_medical_statemachine_cardiacArrestTime = 300;
force force ace_medical_statemachine_fatalInjuriesAI = 0;
force force ace_medical_statemachine_fatalInjuriesPlayer = 0;
force force ace_medical_treatment_advancedBandages = 2;
force force ace_medical_treatment_advancedDiagnose = 2;
force force ace_medical_treatment_advancedMedication = true;
force force ace_medical_treatment_allowBodyBagUnconscious = false;
force force ace_medical_treatment_allowGraveDigging = 2;
force force ace_medical_treatment_allowLitterCreation = true;
force force ace_medical_treatment_allowSelfIV = 1;
force force ace_medical_treatment_allowSelfPAK = 0;
force force ace_medical_treatment_allowSelfStitch = 0;
force force ace_medical_treatment_allowSharedEquipment = 0;
force force ace_medical_treatment_bandageEffectiveness = 1;
force force ace_medical_treatment_bandageRollover = true;
force force ace_medical_treatment_clearTrauma = 2;
force force ace_medical_treatment_consumePAK = 0;
force force ace_medical_treatment_consumeSurgicalKit = 0;
force force ace_medical_treatment_convertItems = 0;
force force ace_medical_treatment_cprSuccessChanceMax = 0.6;
force force ace_medical_treatment_cprSuccessChanceMin = 0.6;
force force ace_medical_treatment_graveDiggingMarker = true;
force force ace_medical_treatment_holsterRequired = 0;
force force ace_medical_treatment_litterCleanupDelay = 300;
force force ace_medical_treatment_locationEpinephrine = 0;
force force ace_medical_treatment_locationIV = 0;
force force ace_medical_treatment_locationPAK = 0;
force force ace_medical_treatment_locationsBoostTraining = true;
force force ace_medical_treatment_locationSurgicalKit = 0;
force force ace_medical_treatment_maxLitterObjects = 500;
force force ace_medical_treatment_medicEpinephrine = 0;
force force ace_medical_treatment_medicIV = 1;
force force ace_medical_treatment_medicPAK = 1;
force force ace_medical_treatment_medicSurgicalKit = 1;
force force ace_medical_treatment_timeCoefficientPAK = 1;
force force ace_medical_treatment_treatmentTimeAutoinjector = 3;
force force ace_medical_treatment_treatmentTimeBodyBag = 4;
force force ace_medical_treatment_treatmentTimeCoeffZeus = 1;
force force ace_medical_treatment_treatmentTimeCPR = 10;
force force ace_medical_treatment_treatmentTimeGrave = 15;
force force ace_medical_treatment_treatmentTimeIV = 10;
force force ace_medical_treatment_treatmentTimeSplint = 5;
force force ace_medical_treatment_treatmentTimeTourniquet = 3;
force force ace_medical_treatment_woundReopenChance = 1;
force force ace_medical_treatment_woundStitchTime = 2;
force force ace_medical_vitals_simulateSpO2 = false;

// ACE Medical Interface
ace_medical_feedback_bloodVolumeEffectType = 2;
ace_medical_feedback_enableHUDIndicators = true;
ace_medical_feedback_painEffectType = 0;
ace_medical_gui_bloodLossColor_0 = [1,1,1,1];
ace_medical_gui_bloodLossColor_1 = [1,0.95,0.64,1];
ace_medical_gui_bloodLossColor_2 = [1,0.87,0.46,1];
ace_medical_gui_bloodLossColor_3 = [1,0.8,0.33,1];
ace_medical_gui_bloodLossColor_4 = [1,0.72,0.24,1];
ace_medical_gui_bloodLossColor_5 = [1,0.63,0.15,1];
ace_medical_gui_bloodLossColor_6 = [1,0.54,0.08,1];
ace_medical_gui_bloodLossColor_7 = [1,0.43,0.02,1];
ace_medical_gui_bloodLossColor_8 = [1,0.3,0,1];
ace_medical_gui_bloodLossColor_9 = [1,0,0,1];
ace_medical_gui_bodyPartOutlineColor = [1,1,1,1];
ace_medical_gui_damageColor_0 = [1,1,1,1];
ace_medical_gui_damageColor_1 = [0.75,0.95,1,1];
ace_medical_gui_damageColor_2 = [0.62,0.86,1,1];
ace_medical_gui_damageColor_3 = [0.54,0.77,1,1];
ace_medical_gui_damageColor_4 = [0.48,0.67,1,1];
ace_medical_gui_damageColor_5 = [0.42,0.57,1,1];
ace_medical_gui_damageColor_6 = [0.37,0.47,1,1];
ace_medical_gui_damageColor_7 = [0.31,0.36,1,1];
ace_medical_gui_damageColor_8 = [0.22,0.23,1,1];
ace_medical_gui_damageColor_9 = [0,0,1,1];
ace_medical_gui_enableActions = 0;
ace_medical_gui_enableMedicalMenu = 1;
ace_medical_gui_enableSelfActions = true;
force force ace_medical_gui_interactionMenuShowTriage = 1;
force force ace_medical_gui_maxDistance = 3;
ace_medical_gui_openAfterTreatment = true;
ace_medical_gui_peekMedicalInfoReleaseDelay = 5;
ace_medical_gui_peekMedicalOnHit = true;
ace_medical_gui_peekMedicalOnHitDuration = 5;
force force ace_medical_gui_showBleeding = 2;
force force ace_medical_gui_showBloodlossEntry = true;
force force ace_medical_gui_showDamageEntry = true;
force force ace_medical_gui_tourniquetWarning = true;

// ACE Name Tags
force ace_nametags_ambientBrightnessAffectViewDist = 1;
ace_nametags_defaultNametagColor = [0.77,0.51,0.08,1];
ace_nametags_nametagColorBlue = [0.67,0.67,1,1];
ace_nametags_nametagColorGreen = [0.67,1,0.67,1];
ace_nametags_nametagColorMain = [1,1,1,1];
ace_nametags_nametagColorRed = [1,0.67,0.67,1];
ace_nametags_nametagColorYellow = [1,1,0.67,1];
force ace_nametags_playerNamesMaxAlpha = 0.8;
force ace_nametags_playerNamesViewDistance = 5;
force ace_nametags_showCursorTagForVehicles = false;
ace_nametags_showNamesForAI = false;
ace_nametags_showPlayerNames = 1;
ace_nametags_showPlayerRanks = true;
ace_nametags_showSoundWaves = 1;
ace_nametags_showVehicleCrewInfo = true;
ace_nametags_tagSize = 2;

// ACE Nightvision
force force ace_nightvision_aimDownSightsBlur = 0;
force force ace_nightvision_disableNVGsWithSights = false;
force force ace_nightvision_effectScaling = 0;
force force ace_nightvision_fogScaling = 0;
force force ace_nightvision_noiseScaling = 0;
force force ace_nightvision_shutterEffects = false;

// ACE Overheating
force ace_overheating_cookoffCoef = 1;
force ace_overheating_coolingCoef = 1;
ace_overheating_displayTextOnJam = true;
force force ace_overheating_enabled = false;
force ace_overheating_heatCoef = 1;
force ace_overheating_jamChanceCoef = 1;
force ace_overheating_overheatingDispersion = true;
force ace_overheating_overheatingRateOfFire = true;
ace_overheating_particleEffectsAndDispersionDistance = 3000;
ace_overheating_showParticleEffects = false;
ace_overheating_showParticleEffectsForEveryone = false;
force ace_overheating_suppressorCoef = 1;
force ace_overheating_unJamFailChance = 0.1;
force ace_overheating_unJamOnreload = false;
force ace_overheating_unJamOnSwapBarrel = false;

// ACE Pointing
force force ace_finger_enabled = false;
ace_finger_indicatorColor = [0.83,0.68,0.21,0.75];
ace_finger_indicatorForSelf = true;
force ace_finger_maxRange = 20;
force ace_finger_proximityScaling = false;
force ace_finger_sizeCoef = 1;

// ACE Pylons
force force ace_pylons_enabledForZeus = true;
force force ace_pylons_enabledFromAmmoTrucks = true;
force force ace_pylons_rearmNewPylons = true;
force force ace_pylons_requireEngineer = false;
force force ace_pylons_requireToolkit = true;
force force ace_pylons_searchDistance = 15;
force force ace_pylons_timePerPylon = 5;

// ACE Quick Mount
force force ace_quickmount_distance = 3;
force force ace_quickmount_enabled = true;
ace_quickmount_enableMenu = 3;
ace_quickmount_priority = 3;
force force ace_quickmount_speed = 20;

// ACE Repair
force ace_repair_addSpareParts = true;
force ace_repair_autoShutOffEngineWhenStartingRepair = true;
force ace_repair_consumeItem_toolKit = 0;
ace_repair_displayTextOnRepair = true;
force ace_repair_enabled = true;
force ace_repair_engineerSetting_fullRepair = 0;
force ace_repair_engineerSetting_repair = 0;
force ace_repair_engineerSetting_wheel = 0;
force ace_repair_fullRepairLocation = 3;
force ace_repair_fullRepairRequiredItems = ["ace_repair_anyToolKit"];
force ace_repair_locationsBoostTraining = false;
force ace_repair_miscRepairRequiredItems = ["ace_repair_anyToolKit"];
force ace_repair_miscRepairTime = 5;
force ace_repair_patchWheelEnabled = 0;
force ace_repair_patchWheelLocation = ["ground","vehicle"];
force ace_repair_patchWheelMaximumRepair = 0.3;
force ace_repair_patchWheelRequiredItems = ["ace_repair_anyToolKit"];
force ace_repair_patchWheelTime = 5;
force ace_repair_repairDamageThreshold = 0.6;
force ace_repair_repairDamageThreshold_engineer = 0.4;
force ace_repair_timeCoefficientFullRepair = 1;
force ace_repair_wheelChangeTime = 10;
force ace_repair_wheelRepairRequiredItems = [];

// ACE Respawn
force ace_respawn_removeDeadBodiesDisconnected = true;
force ace_respawn_savePreDeathGear = false;

// ACE Scopes
force ace_scopes_correctZeroing = true;
force ace_scopes_deduceBarometricPressureFromTerrainAltitude = false;
force ace_scopes_defaultZeroRange = 100;
force force ace_scopes_enabled = false;
force ace_scopes_forceUseOfAdjustmentTurrets = false;
ace_scopes_inScopeAdjustment = false;
force ace_scopes_overwriteZeroRange = false;
force ace_scopes_simplifiedZeroing = false;
ace_scopes_useLegacyUI = false;
force ace_scopes_zeroReferenceBarometricPressure = 1013.25;
force ace_scopes_zeroReferenceHumidity = 0;
force ace_scopes_zeroReferenceTemperature = 15;

// ACE Sitting
force acex_sitting_enable = true;

// ACE Spectator
force ace_spectator_enableAI = false;
ace_spectator_maxFollowDistance = 5;
force ace_spectator_restrictModes = 0;
force ace_spectator_restrictVisions = 0;

// ACE Switch Units
force ace_switchunits_enableSafeZone = true;
force ace_switchunits_enableSwitchUnits = false;
force ace_switchunits_safeZoneRadius = 0;
force ace_switchunits_switchToCivilian = false;
force ace_switchunits_switchToEast = false;
force ace_switchunits_switchToIndependent = false;
force ace_switchunits_switchToWest = false;

// ACE Trenches
force ace_trenches_bigEnvelopeDigDuration = 25;
force ace_trenches_bigEnvelopeRemoveDuration = 15;
force ace_trenches_smallEnvelopeDigDuration = 20;
force ace_trenches_smallEnvelopeRemoveDuration = 12;

// ACE Uncategorized
force ace_fastroping_autoAddFRIES = false;
force ace_fastroping_requireRopeItems = false;
force ace_gunbag_swapGunbagEnabled = true;
force force ace_hitreactions_minDamageToTrigger = 0.596934;
force force ace_hitreactions_weaponDropChanceArmHitAI = 0;
force force ace_hitreactions_weaponDropChanceArmHitPlayer = 0;
ace_inventory_inventoryDisplaySize = 0;
force ace_laser_dispersionCount = 2;
force ace_laser_showLaserOnMap = 2;
force ace_marker_flags_placeAnywhere = false;
force ace_microdagr_mapDataAvailable = 2;
force ace_microdagr_waypointPrecision = 3;
force ace_noradio_enabled = true;
ace_optionsmenu_showNewsOnMainMenu = true;
force ace_overpressure_backblastDistanceCoefficient = 1;
force ace_overpressure_overpressureDistanceCoefficient = 1;
force ace_parachute_failureChance = 0;
ace_parachute_hideAltimeter = false;
ace_tagging_quickTag = 0;

// ACE User Interface
force ace_ui_allowSelectiveUI = true;
ace_ui_ammoCount = false;
ace_ui_ammoType = true;
ace_ui_commandMenu = true;
force ace_ui_enableSpeedIndicator = true;
ace_ui_firingMode = true;
ace_ui_groupBar = false;
ace_ui_gunnerAmmoCount = true;
ace_ui_gunnerAmmoType = true;
ace_ui_gunnerFiringMode = true;
ace_ui_gunnerLaunchableCount = true;
ace_ui_gunnerLaunchableName = true;
ace_ui_gunnerMagCount = true;
ace_ui_gunnerWeaponLowerInfoBackground = true;
ace_ui_gunnerWeaponName = true;
ace_ui_gunnerWeaponNameBackground = true;
ace_ui_gunnerZeroing = true;
ace_ui_hideDefaultActionIcon = false;
ace_ui_magCount = true;
ace_ui_soldierVehicleWeaponInfo = true;
ace_ui_staminaBar = true;
ace_ui_stance = true;
ace_ui_throwableCount = true;
ace_ui_throwableName = true;
ace_ui_vehicleAltitude = true;
ace_ui_vehicleCompass = true;
ace_ui_vehicleDamage = true;
ace_ui_vehicleFuelBar = true;
ace_ui_vehicleInfoBackground = true;
ace_ui_vehicleName = true;
ace_ui_vehicleNameBackground = true;
ace_ui_vehicleRadar = true;
ace_ui_vehicleSpeed = true;
ace_ui_weaponLowerInfoBackground = true;
ace_ui_weaponName = true;
ace_ui_weaponNameBackground = true;
ace_ui_zeroing = true;

// ACE Vehicle Lock
force ace_vehiclelock_defaultLockpickStrength = 10;
force ace_vehiclelock_lockVehicleInventory = false;
force ace_vehiclelock_vehicleStartingLockState = -1;

// ACE Vehicles
force ace_novehicleclanlogo_enabled = false;
ace_vehicles_hideEjectAction = true;
force ace_vehicles_keepEngineRunning = false;
ace_vehicles_speedLimiterStep = 5;
force ace_viewports_enabled = true;

// ACE View Distance Limiter
force ace_viewdistance_enabled = false;
force ace_viewdistance_limitViewDistance = 12000;
ace_viewdistance_objectViewDistanceCoeff = 0;
ace_viewdistance_viewDistanceAirVehicle = 0;
ace_viewdistance_viewDistanceLandVehicle = 0;
ace_viewdistance_viewDistanceOnFoot = 0;

// ACE View Restriction
force acex_viewrestriction_mode = 0;
force acex_viewrestriction_modeSelectiveAir = 0;
force acex_viewrestriction_modeSelectiveFoot = 0;
force acex_viewrestriction_modeSelectiveLand = 0;
force acex_viewrestriction_modeSelectiveSea = 0;
acex_viewrestriction_preserveView = false;

// ACE Volume
acex_volume_enabled = false;
acex_volume_fadeDelay = 1;
acex_volume_lowerInVehicles = false;
acex_volume_reduction = 5;
acex_volume_remindIfLowered = false;
acex_volume_showNotification = true;

// ACE Weapons
ace_common_persistentLaserEnabled = false;
ace_reload_displayText = true;
ace_reload_showCheckAmmoSelf = false;
ace_reloadlaunchers_displayStatusText = true;
ace_weaponselect_displayText = true;

// ACE Weather
force ace_weather_enabled = true;
ace_weather_showCheckAirTemperature = true;
force ace_weather_updateInterval = 60;
force ace_weather_windSimulation = true;

// ACE Wind Deflection
force ace_winddeflection_enabled = true;
force ace_winddeflection_simulationInterval = 0.05;
force ace_winddeflection_vehicleEnabled = true;

// ACE Zeus
force ace_zeus_autoAddObjects = false;
force ace_zeus_canCreateZeus = 0;
force ace_zeus_radioOrdnance = false;
force ace_zeus_remoteWind = false;
force ace_zeus_revealMines = 0;
force ace_zeus_zeusAscension = true;
force ace_zeus_zeusBird = false;

// Aux501
Aux501_jumppack_setting_cycleHide_Cancel = true;
Aux501_jumppack_setting_cycleHide_Dash = true;
Aux501_jumppack_setting_cycleHide_Forward = false;
Aux501_jumppack_setting_cycleHide_Short = false;
Aux501_LowLight_DefaultGain = 500;

// Community Base Addons
cba_diagnostic_ConsoleIndentType = -1;
force cba_diagnostic_watchInfoRefreshRate = 0.2;
cba_disposable_dropUsedLauncher = 2;
force cba_disposable_replaceDisposableLauncher = true;
cba_events_repetitionMode = 1;
force cba_network_loadoutValidation = 0;
cba_optics_usePipOptics = true;
cba_ui_notifyLifetime = 4;
cba_ui_StorePasswords = 1;

// DBA CIS
force DBA_B2Revive = true;
force DBA_B2ReviveChanceOverride = 0.2;
force DBA_B2ReviveOverride = false;
force DBA_BuffLevel = 1;
force DBA_CentralComputerBuff = true;
force DBA_CentralComputerBuffOther = 0;
force DBA_CentralComputerDisable = 0;
force DBA_CentralComputerEnable = 0;
force DBA_CentralComputerSwitch = true;
force DBA_DisabledSuperTacticalDroidSystem = 0.5;
force DBA_DisabledSystem = 0.5;
force DBA_DisabledTacticalDroidSystem = 0.5;
force DBA_OOMStatus = false;
force DBA_SuperTacticalBuffLevel = 0.95;
force DBA_SuperTacticalDroidBuff = true;
force DBA_SuperTacticalDroidBuffOther = 2;
force DBA_TacticalBuffLevel = 0.85;
force DBA_TacticalDroidBuff = true;
force DBA_TacticalDroidBuffOther = 2;

// DBA Core
force DBA_IgniteArmoured = true;
force DBA_IgniteDroids = true;
force DBA_IgniteEnable = true;
force DBA_IgniteThermallyInsulated = false;
force DBA_JammerBaseRange = 2000;
force DBA_OverLoadTimer = 2;
force DBA_PowerAmplification = 1;
force DBA_StunArmoured = false;
force DBA_StunDroids = false;
force DBA_StunDuration = 5;
force DBA_StunEnable = true;
force DBA_StunHeavyDroids = false;

// DevourerKing Common
dev_cba_damageMultiplier = 1;
dev_cba_friendly = "[""VirtualCurator_F"",""""B_VirtualCurator_F"",""""O_VirtualCurator_F"",""""I_VirtualCurator_F"",""""C_VirtualCurator_F""]";
force dev_cba_friendlySide = true;
force dev_cba_killswitch = false;
force dev_zombie_deleteWeapon = true;

// DUI - Squad Radar - Indicators
force diwako_dui_indicators_crew_range_enabled = false;
diwako_dui_indicators_fov_scale = false;
diwako_dui_indicators_icon_buddy = true;
diwako_dui_indicators_icon_leader = true;
diwako_dui_indicators_icon_medic = true;
diwako_dui_indicators_range = 20;
diwako_dui_indicators_range_crew = 300;
diwako_dui_indicators_range_scale = false;
diwako_dui_indicators_show = true;
diwako_dui_indicators_size = 1;
diwako_dui_indicators_style = "standard";
diwako_dui_indicators_useACENametagsRange = true;

// DUI - Squad Radar - Main
diwako_dui_ace_hide_interaction = true;
diwako_dui_colors = "standard";
diwako_dui_font = "RobotoCondensed";
diwako_dui_icon_style = "standard";
diwako_dui_main_hide_dialog = true;
diwako_dui_main_hide_ui_by_default = false;
diwako_dui_main_squadBlue = [0,0,1,1];
diwako_dui_main_squadGreen = [0,1,0,1];
diwako_dui_main_squadMain = [1,1,1,1];
diwako_dui_main_squadRed = [1,0,0,1];
diwako_dui_main_squadYellow = [1,1,0,1];
diwako_dui_main_trackingColor = [0.93,0.26,0.93,1];
diwako_dui_reset_ui_pos = false;

// DUI - Squad Radar - Nametags
diwako_dui_nametags_customRankStyle = "[[""PRIVATE"",""CORPORAL"",""SERGEANT"",""LIEUTENANT"",""CAPTAIN"",""MAJOR"",""COLONEL""],[""Pvt."",""Cpl."",""Sgt."",""Lt."",""Capt."",""Maj."",""Col.""]]";
diwako_dui_nametags_deadColor = [0.2,0.2,0.2,1];
diwako_dui_nametags_deadRenderDistance = 3.5;
diwako_dui_nametags_drawRank = true;
diwako_dui_nametags_enabled = true;
diwako_dui_nametags_enableFOVBoost = true;
diwako_dui_nametags_enableOcclusion = true;
diwako_dui_nametags_fadeInTime = 0.05;
diwako_dui_nametags_fadeOutTime = 0.5;
diwako_dui_nametags_fontGroup = "RobotoCondensedLight";
diwako_dui_nametags_fontGroupNameSize = 8;
diwako_dui_nametags_fontName = "RobotoCondensedBold";
diwako_dui_nametags_fontNameSize = 10;
diwako_dui_nametags_groupColor = [1,1,1,1];
diwako_dui_nametags_groupFontShadow = 1;
diwako_dui_nametags_groupNameOtherGroupColor = [0.6,0.85,0.6,1];
diwako_dui_nametags_nameFontShadow = 1;
diwako_dui_nametags_nameOtherGroupColor = [0.2,1,0,1];
diwako_dui_nametags_rankNameStyle = "default";
diwako_dui_nametags_renderDistance = 40;
diwako_dui_nametags_showUnconAsDead = true;
diwako_dui_nametags_useLIS = true;
diwako_dui_nametags_useSideIsFriendly = true;

// DUI - Squad Radar - Radar
diwako_dui_compass_hide_alone_group = false;
diwako_dui_compass_hide_blip_alone_group = false;
diwako_dui_compass_icon_scale = 1;
diwako_dui_compass_opacity = 1;
diwako_dui_compass_style = ["\z\diwako_dui\addons\radar\UI\compass_styles\standard\compass_limited.paa","\z\diwako_dui\addons\radar\UI\compass_styles\standard\compass.paa"];
diwako_dui_compassRange = 35;
diwako_dui_compassRefreshrate = 0;
diwako_dui_dir_showMildot = false;
diwako_dui_dir_size = 1.25;
diwako_dui_distanceWarning = 3;
diwako_dui_enable_compass = true;
diwako_dui_enable_compass_dir = 1;
diwako_dui_enable_occlusion = false;
diwako_dui_enable_occlusion_cone = 360;
diwako_dui_hudScaling = 1;
diwako_dui_namelist = true;
diwako_dui_namelist_bg = 0;
diwako_dui_namelist_only_buddy_icon = false;
diwako_dui_namelist_size = 1;
diwako_dui_namelist_text_shadow = 2;
diwako_dui_namelist_width = 215;
diwako_dui_radar_ace_finger = true;
force diwako_dui_radar_ace_medic = true;
diwako_dui_radar_always_show_unit_numbers = false;
diwako_dui_radar_compassRangeCrew = 500;
diwako_dui_radar_dir_padding = 25;
diwako_dui_radar_dir_shadow = 2;
diwako_dui_radar_enable_seat_icons = 2;
diwako_dui_radar_group_by_vehicle = false;
diwako_dui_radar_icon_opacity = 1;
diwako_dui_radar_icon_opacity_no_player = true;
force diwako_dui_radar_icon_priority_setting = 1;
diwako_dui_radar_icon_scale_crew = 6;
diwako_dui_radar_leadingZeroes = false;
diwako_dui_radar_namelist_hideWhenLeader = false;
diwako_dui_radar_namelist_vertical_spacing = 1;
diwako_dui_radar_occlusion_fade_in_time = 1;
diwako_dui_radar_occlusion_fade_time = 10;
diwako_dui_radar_pointer_color = [1,0.5,0,1];
diwako_dui_radar_pointer_style = "standard";
diwako_dui_radar_show_cardinal_points = true;
diwako_dui_radar_showSpeaking = true;
diwako_dui_radar_showSpeaking_radioOnly = false;
diwako_dui_radar_showSpeaking_replaceIcon = true;
force diwako_dui_radar_sortType = "none";
force diwako_dui_radar_sqlFirst = false;
force diwako_dui_radar_syncGroup = false;
force diwako_dui_radar_vehicleCompassEnabled = false;
diwako_dui_use_layout_editor = false;

// Freestyles Crash Landing
force fscl501_damageTreshold = 99;
force force fscl501_debug = false;
force force fscl501_ejectionProp = 100;
force force fscl501_ejectionSystem = true;
force force fscl501_gForceThreshold = 0;
force force fscl501_ignoreNonPlayerVehicles = true;
force force fscl501_stateThreshold = 1;

// GRAD Trenches
force grad_trenches_functions_allowBigEnvelope = true;
force force grad_trenches_functions_allowCamouflage = true;
force force grad_trenches_functions_allowDigging = true;
force force grad_trenches_functions_allowEffects = true;
force force grad_trenches_functions_allowGiantEnvelope = true;
force force grad_trenches_functions_allowHitDecay = false;
force force grad_trenches_functions_allowLongEnvelope = true;
force force grad_trenches_functions_allowShortEnvelope = true;
force force grad_trenches_functions_allowSmallEnvelope = true;
force force grad_trenches_functions_allowTextureLock = true;
force force grad_trenches_functions_allowTrenchDecay = false;
force force grad_trenches_functions_allowVehicleEnvelope = true;
force force grad_trenches_functions_bigEnvelopeDamageMultiplier = 2;
force force grad_trenches_functions_bigEnvelopeDigTime = 40;
force force grad_trenches_functions_bigEnvelopeRemovalTime = -1;
force force grad_trenches_functions_buildFatigueFactor = 0;
force force grad_trenches_functions_camouflageRequireEntrenchmentTool = true;
force force grad_trenches_functions_createTrenchMarker = false;
force force grad_trenches_functions_decayTime = 1800;
force force grad_trenches_functions_giantEnvelopeDamageMultiplier = 1;
force force grad_trenches_functions_giantEnvelopeDigTime = 90;
force force grad_trenches_functions_giantEnvelopeRemovalTime = -1;
force force grad_trenches_functions_hitDecayMultiplier = 0.334378;
force force grad_trenches_functions_LongEnvelopeDigTime = 100;
force force grad_trenches_functions_LongEnvelopeRemovalTime = -1;
force force grad_trenches_functions_playersInAreaRadius = 0;
force force grad_trenches_functions_shortEnvelopeDamageMultiplier = 2;
force force grad_trenches_functions_shortEnvelopeDigTime = 15;
force force grad_trenches_functions_shortEnvelopeRemovalTime = -1;
force force grad_trenches_functions_smallEnvelopeDamageMultiplier = 3;
force force grad_trenches_functions_smallEnvelopeDigTime = 30;
force force grad_trenches_functions_smallEnvelopeRemovalTime = -1;
force force grad_trenches_functions_stopBuildingAtFatigueMax = false;
force force grad_trenches_functions_textureLockDistance = 5;
force force grad_trenches_functions_timeoutToDecay = 7200;
force force grad_trenches_functions_vehicleEnvelopeDamageMultiplier = 1;
force force grad_trenches_functions_vehicleEnvelopeDigTime = 120;
force force grad_trenches_functions_vehicleEnvelopeRemovalTime = -1;
force force grad_trenches_functions_vehicleTrenchBuildSpeed = 5;

// JLTS - Debug
JLTS_settings_Debug_chat = true;
force JLTS_settings_Debug_mainSwitch = 0;
JLTS_settings_Debug_rpt = true;

// JLTS - Special equipment
force JLTS_settings_jumppack_customConsumption = false;
force JLTS_settings_jumppack_customConsumptionCoef = 1;
force JLTS_settings_jumppack_mainSwitch = 0;
JLTS_settings_jumppack_profileDescentCoef = 0.3;
JLTS_settings_jumppack_profileDescentRatio = "1,1";
JLTS_settings_jumppack_profileHighCoef = 1;
JLTS_settings_jumppack_profileHighRatio = "1,2";
JLTS_settings_jumppack_profileLongCoef = 1;
JLTS_settings_jumppack_profileLongRatio = "2,1";
JLTS_settings_jumppack_profileStandardCoef = 1;
JLTS_settings_jumppack_profileStandardRatio = "1,1";
force JLTS_settings_jumppack_stances = 1;

// JLTS - Weapons
force JLTS_settings_Common_dropShield = true;
force JLTS_settings_EMP_EMPEffectScope = 2;
force JLTS_settings_EMP_mainSwitch = 1;
JLTS_settings_EMP_notifyPlayer = true;
force JLTS_settings_EMP_repairTimeHandgun = 20;
force JLTS_settings_EMP_repairTimePrimary = 30;
force JLTS_settings_EMP_repairTimeSecondary = 40;
force JLTS_settings_Stun_mainSwitch = 1;
force JLTS_settings_Stun_worksInVehicles = true;

// KAT - ADV Medical: Airway
force force kat_airway_Accuvac_time = 8;
force force kat_airway_autoTriage = true;
force force kat_airway_block_headTurning_ifAirwayItem = true;
force force kat_airway_CancelRecoveryPosition_Time = 6;
force force kat_airway_CheckAirway_time = 2;
force force kat_airway_checkbox_puking_sound = false;
force force kat_airway_enable = false;
force force kat_airway_Guedeltubus_time = 6;
force force kat_airway_HeadTurn_Interval = 3;
force force kat_airway_Hyperextend_Time = 3;
force force kat_airway_Larynxtubus_time = 3;
force force kat_airway_medLvl_Accuvac = 0;
force force kat_airway_medLvl_Guedeltubus = 0;
force force kat_airway_medLvl_Larynxtubus = 0;
force force kat_airway_medLvl_Suction = 0;
force force kat_airway_occlusion_cooldownPeriod = 6;
force force kat_airway_occlusion_repeatTimer = 60;
force force kat_airway_probability_headturning = 50;
force force kat_airway_probability_obstruction = 15;
force force kat_airway_probability_occluded = 10;
force force kat_airway_RecoveryPosition_Time = 6;
force force kat_airway_RecoveryPosition_TimeToDrain = 10;
force force kat_airway_ReusableAirwayItems = false;
force force kat_airway_string_exit = "";
force force kat_airway_Suction_reuse = false;
force force kat_airway_Suction_time = 12;

// KAT - ADV Medical: Breathing
force force kat_breathing_advPtxChance = 5;
force force kat_breathing_advPtxEnable = false;
force force kat_breathing_arrestPneumothorax_interval = 30;
force force kat_breathing_BVMOxygen_Multiplier = 1;
force force kat_breathing_clearChestSealAfterTreatment = false;
force force kat_breathing_deepPenetratingInjuryChance = 30;
force force kat_breathing_deterioratingPneumothorax_chance = 10;
force force kat_breathing_deterioratingPneumothorax_interval = 60;
force force kat_breathing_enable = false;
force force kat_breathing_enable_selfChestseal = 1;
force force kat_breathing_Etco2_Enabled = true;
force force kat_breathing_Etco2_Units = 0;
force force kat_breathing_HPTXBleedAmount = 0.06;
force force kat_breathing_hptxChance = 5;
force force kat_breathing_inspectChest_enable = 2;
force force kat_breathing_inspectChest_medLvl = 0;
force force kat_breathing_inspectChest_time = 6;
force force kat_breathing_locationProvideOxygen = 3;
force force kat_breathing_medLvl_BVM = 0;
force force kat_breathing_medLvl_BVM_Oxygen = 0;
force force kat_breathing_medLvl_Chestseal = 0;
force force kat_breathing_medLvl_hemopneumothoraxTreatment = 0;
force force kat_breathing_medLvl_NasalCannula = 0;
force force kat_breathing_medLvl_PocketBVM = 0;
force force kat_breathing_medLvl_Pulseoximeter = 0;
force force kat_breathing_mildValue = 75;
force force kat_breathing_NasalCannula_time = 3;
force force kat_breathing_paco2Active = false;
force force kat_breathing_PneumothoraxAlwaysVisible = false;
force force kat_breathing_PneumothoraxArrest = true;
force force kat_breathing_pneumothoraxChance = 5;
force force kat_breathing_pneumothoraxDamageThreshold = 0.4;
force force kat_breathing_pneumothoraxDamageThreshold_TakenDamage = true;
force force kat_breathing_PortableOxygenTank_RefillTime = 5;
force force kat_breathing_PulseOximeter_SpO2Warning = 85;
force force kat_breathing_severeValue = 66;
force force kat_breathing_showCyanosis = false;
force force kat_breathing_showPneumothorax_dupe = false;
force force kat_breathing_slightValue = 90;
force force kat_breathing_SpO2_cardiacActive = false;
force force kat_breathing_SpO2_cardiacValue = 75;
force force kat_breathing_SpO2_dieActive = false;
force force kat_breathing_SpO2_dieValue = 65;
force force kat_breathing_SpO2_MultiplyNegative = 1;
force force kat_breathing_SpO2_MultiplyPositive = 1;
force force kat_breathing_SpO2_perfusion = false;
force force kat_breathing_SpO2_PerfusionMultiplier = 1;
force force kat_breathing_SpO2_unconscious = 75;
force force kat_breathing_Stable_spo2 = 85;
force force kat_breathing_staminaLossAtLowSPO2 = true;
force force kat_breathing_stethoscopeListeningTime = 15;
kat_breathing_stethoscopeSoundVolume = 2;
force force kat_breathing_TensionHemothoraxAlwaysVisible = false;

// KAT - ADV Medical: Chemical
force force kat_chemical_affectAI = false;
force force kat_chemical_availGasmask = "'G_AirPurifyingRespirator_01_F','kat_mask_M50','kat_mask_M04','RD501_Diving_Goggles'";
force force kat_chemical_gasmask_durability = 900;
force force kat_chemical_infectionTime = 60;

// KAT - ADV Medical: Circulation
force force kat_circulation_AdvRhythm = false;
force force kat_circulation_AdvRhythm_AED_ROSC_Chance = 50;
force force kat_circulation_AdvRhythm_asystoleBloodlossThreshold = 3.6;
force force kat_circulation_AdvRhythm_canDeteriorate = true;
force force kat_circulation_AdvRhythm_CPR_ROSC_Chance = 5;
force force kat_circulation_AdvRhythm_deteriorateAfterTreatment = true;
force force kat_circulation_AdvRhythm_deteriorateTimeMax = 900;
force force kat_circulation_AdvRhythm_deteriorateTimeWeight = 180;
force force kat_circulation_AdvRhythm_Hardcore_Enable = false;
force force kat_circulation_AdvRhythm_hardcoreDeteriorationChance = 10;
force force kat_circulation_AdvRhythm_PEAChance = 50;
force force kat_circulation_AdvRhythm_VTChance = 50;
force force kat_circulation_AED_duringCpr = true;
force force kat_circulation_AED_MaxChance = 80;
force force kat_circulation_AED_MinChance = 80;
force force kat_circulation_AED_X_MaxChance = 90;
force force kat_circulation_AED_X_MinChance = 90;
force force kat_circulation_AED_X_Monitor_SpO2Warning = 85;
force force kat_circulation_AED_X_VitalsMonitor_BloodPressureInterval = 0;
force force kat_circulation_AED_X_VitalsMonitor_BloodPressureInterval_Time = 30;
force force kat_circulation_AED_X_VitalsMonitor_SoundsSelect = 1;
force force kat_circulation_AEDX_VitalsMonitor_AttachTime = 2;
force force kat_circulation_AEDX_VitalsMonitor_DetachTime = 2;
force force kat_circulation_blood_draw_limit = 3.6;
force force kat_circulation_blood_drawTime_250ml = 5;
force force kat_circulation_blood_drawTime_500ml = 10;
force force kat_circulation_bloodGroups = false;
force force kat_circulation_bloodTypeCustomList = """AB""";
force force kat_circulation_bloodTypeRandomWeighted = false;
force force kat_circulation_bloodTypeSetting = 5;
kat_circulation_bloodTypeSettingPlayer = "AB";
force force kat_circulation_cardiacArrestBleedRate = 0.05;
force force kat_circulation_CPR_ChanceInterval = 12;
force force kat_circulation_CPR_MaxChance_Default = 50;
force force kat_circulation_CPR_MaxChance_Doctor = 80.1618;
force force kat_circulation_CPR_MaxChance_RegularMedic = 70;
force force kat_circulation_CPR_MinChance_Default = 50;
force force kat_circulation_CPR_MinChance_Doctor = 80;
force force kat_circulation_CPR_MinChance_RegularMedic = 70;
force force kat_circulation_CPR_OxygenationPeriod = 15;
force force kat_circulation_Defibrillator_DistanceLimit = 6;
force force kat_circulation_DefibrillatorPads_AttachTime = 6;
force force kat_circulation_DefibrillatorPads_DetachTime = 3;
force force kat_circulation_deterioratingTamponade_chance = 35;
force force kat_circulation_deterioratingTamponade_interval = 60;
force force kat_circulation_enable = true;
force force kat_circulation_enable_CPR_Chances = true;
force force kat_circulation_enable_selfBloodDraw = 1;
force force kat_circulation_medLvl_AED = 0;
force force kat_circulation_medLvl_AED_Station_Interact = 0;
force force kat_circulation_medLvl_AED_X = 1;
force force kat_circulation_tamponadeChance = 10;
force force kat_circulation_useLocation_AED = 0;

// KAT - ADV Medical: Feedback
force force kat_feedback_effectLowSpO2 = 90;
force force kat_feedback_enableOpioidEffect = false;

// KAT - ADV Medical: GUI
force force kat_gui_ColoredLogs = true;
force force kat_gui_showPatientSideLabels = true;

// KAT - ADV Medical: Hypothermia
force force kat_hypothermia_hypothermiaActive = false;

// KAT - ADV Medical: Misc
kat_misc_AFAK_Container = 0;
kat_misc_AFAK_Item_Color = [0.67,0.84,0.9];
force force kat_misc_AFAK_RemoveWhenEmpty = true;
kat_misc_AFAK_Slot_Color = [1,0.96,0.32];
force force kat_misc_AFAKFifthSlotItem = "[['ACE_splint', 4]]";
force force kat_misc_AFAKFirstSlotItem = "[['ACE_elasticBandage', 15]]";
force force kat_misc_AFAKFourthSlotItem = "[['ACE_epinephine', 3]]";
force force kat_misc_AFAKSecondSlotItem = "[['ACE_packingBandage', 10]]";
force force kat_misc_AFAKSixthSlotItem = "[['ACE_plasmaIV_500', 2]]";
force force kat_misc_AFAKThirdSlotItem = "[['kat_PainkillerItem', 1], ['RD501_Painkiller', 3]]";
force force kat_misc_allowSharedVehicleEquipment = 4;
kat_misc_armbandSlingLeftArm = "0.5,-0.41,-0.2";
kat_misc_armbandSlingLeftArmRotation = "240,33,26";
kat_misc_armbandSlingLeftLeg = "0.435,-0.075,-0.38";
kat_misc_armbandSlingLeftLegRotation = "-160,-5,45";
kat_misc_armbandSlingRightArm = "-0.228,-0.1,-0.43";
kat_misc_armbandSlingRightArmRotation = "5,-5,-5";
kat_misc_armbandSlingRightLeg = "-0.32,-0.29,-0.42";
kat_misc_armbandSlingRightLegRotation = "-30,-5,38";
force force kat_misc_enable = true;
force force kat_misc_enableStitchFullBody = true;
kat_misc_IFAK_Container = 0;
kat_misc_IFAK_Item_Color = [0.67,0.84,0.9];
force force kat_misc_IFAK_RemoveWhenEmpty = true;
kat_misc_IFAK_Slot_Color = [1,0.3,0.3];
force force kat_misc_IFAKFirstSlotItem = "[['ACE_elasticBandage', 5]]";
force force kat_misc_IFAKFourthSlotItem = "[['ACE_splint', 4]]";
force force kat_misc_IFAKSecondSlotItem = "[['ACE_packingBandage', 15]]";
force force kat_misc_IFAKThirdSlotItem = "[['kat_PainkillerItem', 1], ['RD501_Painkiller', 2]]";
force force kat_misc_incompatibilityWarning = true;
kat_misc_MFAK_Container = 0;
kat_misc_MFAK_Item_Color = [0.67,0.84,0.9];
force force kat_misc_MFAK_RemoveWhenEmpty = true;
kat_misc_MFAK_Slot_Color = [0.56,0.93,0.56];
force force kat_misc_MFAKEighthSlotItem = "[['kat_nitroglycerin', 6]]";
force force kat_misc_MFAKFifthSlotItem = "[['ACE_plasmaIV_500', 7]]";
force force kat_misc_MFAKFirstSlotItem = "[['ACE_elasticBandage', 65]]";
force force kat_misc_MFAKFourthSlotItem = "[['ACE_epinephine', 5]]";
force force kat_misc_MFAKSecondSlotItem = "[['ACE_packingBandage', 35]]";
force force kat_misc_MFAKSeventhSlotItem = "[['kat_phenylephrine', 6]]";
force force kat_misc_MFAKSixthSlotItem = "[['ACE_plasmaIV_1000', 6]]";
force force kat_misc_MFAKThirdSlotItem = "[['RD501_Painkiller', 10]]";
force kat_misc_tourniquetEffects_Enable = false;
force kat_misc_tourniquetEffects_NegativeMultiplier = 1.2;
force kat_misc_tourniquetEffects_PositiveMultiplier = 0.5;
force force kat_misc_treatmentTimeDetachTourniquet = 2;

// KAT - ADV Medical: Pharmacy
force force kat_pharma_allowStackScript_EACA = true;
force force kat_pharma_allowStackScript_TXA = true;
force force kat_pharma_bandageCycleTime_EACA = 8;
force force kat_pharma_bandageCycleTime_TXA = 5;
force force kat_pharma_blockChance = 0;
force force kat_pharma_carbonateChance = 100;
force force kat_pharma_CheckCoag_Location = 0;
force force kat_pharma_CheckCoag_MedLevel = 2;
force force kat_pharma_CheckCoag_TreatmentTime = 4;
force force kat_pharma_coagulation = true;
force force kat_pharma_coagulation_allow_clot_text = true;
force force kat_pharma_coagulation_allow_EACA_script = true;
force force kat_pharma_coagulation_allow_LargeWounds = true;
force force kat_pharma_coagulation_allow_MediumWounds = true;
force force kat_pharma_coagulation_allow_MinorWounds = true;
force force kat_pharma_coagulation_allow_TXA_script = true;
force force kat_pharma_coagulation_allowOnAI = false;
force force kat_pharma_coagulation_factor_count = 48;
force force kat_pharma_coagulation_factor_limit = 200;
force force kat_pharma_coagulation_factor_regenerate_time = 60;
force force kat_pharma_coagulation_on_all_Bodyparts = true;
force force kat_pharma_coagulation_requireBV = 3.6;
force force kat_pharma_coagulation_requireHR = true;
force force kat_pharma_coagulation_time = 10;
force force kat_pharma_coagulation_time_large = 45;
force force kat_pharma_coagulation_time_medium = 30;
force force kat_pharma_coagulation_time_minor = 15;
force force kat_pharma_coagulation_tourniquetBlock = false;
force force kat_pharma_eacaClearTrauma = true;
force force kat_pharma_ivCheckLimbDamage = false;
force force kat_pharma_IVdrop = 1200;
force force kat_pharma_IVdropEnable = false;
force force kat_pharma_IVflowControl = false;
force force kat_pharma_IVreuse = true;
force force kat_pharma_keepScriptRunning_EACA = true;
force force kat_pharma_keepScriptRunning_TXA = true;
force force kat_pharma_kidneyAction = false;
force force kat_pharma_MedicationsRequireInsIV = true;
force force kat_pharma_medLvl_Amiodarone = 1;
force force kat_pharma_medLvl_ApplyIO = 1;
force force kat_pharma_medLvl_ApplyIV = 1;
force force kat_pharma_medLvl_Atropine = 0;
force force kat_pharma_medLvl_Carbonate = 0;
force force kat_pharma_medLvl_EACA = 2;
force force kat_pharma_medLvl_EpinephrineIV = 1;
force force kat_pharma_medLvl_Etomidate = 1;
force force kat_pharma_medLvl_Fentanyl = 1;
force force kat_pharma_medLvl_Flumezenil = 2;
force force kat_pharma_medLvl_Ketamine = 2;
force force kat_pharma_medLvl_Lidocaine = 1;
force force kat_pharma_medLvl_Lorazepam = 2;
force force kat_pharma_medLvl_Nalbuphine = 1;
force force kat_pharma_medLvl_Naloxone = 0;
force force kat_pharma_medLvl_Nitroglycerin = 1;
force force kat_pharma_medLvl_Norepinephrine = 1;
force force kat_pharma_medLvl_Penthrox = 0;
force force kat_pharma_medLvl_Pervitin = 2;
force force kat_pharma_medLvl_Phenylephrine = 1;
force force kat_pharma_medLvl_Reorientation = 0;
force force kat_pharma_medLvl_TXA = 2;
force force kat_pharma_pervitinSpeed = 1.15;
force force kat_pharma_Reorientation_Enable = true;
force force kat_pharma_Reorientation_Slap = true;
force force kat_pharma_reorientationChance = 89.5316;
force force kat_pharma_RequireInsIV = true;
force force kat_pharma_RequireInsIVBloodDraw = true;
force force kat_pharma_staminaMedication = false;
force force kat_pharma_treatmentTime_Amiodarone = 4;
force force kat_pharma_treatmentTime_ApplyIO = 4;
force force kat_pharma_treatmentTime_ApplyIV = 2;
force force kat_pharma_treatmentTime_Atropine = 4;
force force kat_pharma_treatmentTime_Carbonate = 3;
force force kat_pharma_treatmentTime_EACA = 4;
force force kat_pharma_treatmentTime_EpinephrineIV = 2;
force force kat_pharma_treatmentTime_Etomidate = 2;
force force kat_pharma_treatmentTime_Fentanyl = 6;
force force kat_pharma_treatmentTime_Flumazenil = 2;
force force kat_pharma_treatmentTime_Ketamine = 6;
force force kat_pharma_treatmentTime_Lidocaine = 2;
force force kat_pharma_treatmentTime_Lorazepam = 2;
force force kat_pharma_treatmentTime_Nalbuphine = 4;
force force kat_pharma_treatmentTime_Naloxone = 4;
force force kat_pharma_treatmentTime_Nitroglycerin = 4;
force force kat_pharma_treatmentTime_Norepinephrine = 4;
force force kat_pharma_treatmentTime_Penthrox = 4.0098;
force force kat_pharma_treatmentTime_Pervitin = 6;
force force kat_pharma_treatmentTime_Phenylephrine = 2;
force force kat_pharma_treatmentTime_Reorientation = 2;
force force kat_pharma_treatmentTime_TXA = 4;
force force kat_pharma_weapon_sway_pervitin = true;

// KAT - ADV Medical: Surgery
force force kat_surgery_closedLocation = 0;
force force kat_surgery_closedReduction_MedLevel = 1;
force force kat_surgery_closedReductionFailChance = 10;
force force kat_surgery_closedTime = 3;
force force kat_surgery_compoundChance = 0;
force force kat_surgery_enable_fracture = true;
force force kat_surgery_enable_selfCheckFracture = 1;
force force kat_surgery_etomidateTime = 45;
force force kat_surgery_fractureCheck_MedLevel = 0;
force force kat_surgery_fractureCheck_Time = 2;
force force kat_surgery_incisionTime = 6;
force force kat_surgery_intermediateTime = 6;
force force kat_surgery_npwt_MedLevel = 2;
force force kat_surgery_npwtLocation = 0;
force force kat_surgery_npwtTime = 20;
force force kat_surgery_openTime = 3;
force force kat_surgery_pericardialtap_MedLevel = 2;
force force kat_surgery_pericardialtapLocation = 0;
force force kat_surgery_pericardialtapTime = 2;
force force kat_surgery_reboa_MedLevel = 2;
force force kat_surgery_reboaLocation = 0;
force force kat_surgery_reboaTime = 4;
force force kat_surgery_simpleChance = 100;
force force kat_surgery_Surgery_ConsciousnessRequirement = 2;
force force kat_surgery_surgicalAction_MedLevel = 1;
force force kat_surgery_surgicalLocation = 0;
force force kat_surgery_ultrasound_MedLevel = 2;
force force kat_surgery_ultrasoundLocation = 0;
force force kat_surgery_ultrasoundTime = 2;

// KAT - ADV Medical: Vitals
force force kat_vitals_enableFluidShift = true;
force force kat_vitals_enableSimpleMedical = false;

// KAT - ADV Medical: Watch
force kat_watch_altitudeUnit = 0;
force kat_watch_pressureUnit = 0;
force kat_watch_temperatureUnit = 0;

// Legion Studios
ls_setting_impulseHintDisplay = 0;

// MRHSatellite Options
force MRH_SAT_allowFullscreen = true;
force MRH_SAT_allowLasering = true;
force MRH_SAT_allowTargetDetection = true;
force MRH_SAT_allowTargetTracking = true;
force MRH_SAT_MaxSatAltitude = 300;

// Necroplague
force dev_cba_infection = true;
dev_cba_infection_prolongTime = 900;
dev_cba_infection_resurrectAmugusChance = 0;
dev_cba_infection_resurrectParasiteChance = 0.1;
dev_cba_infection_resurrectTime = "[25,30,35]";
force dev_cba_infection_resurrectWebknight = false;
force dev_cba_infection_resurrectZombie = true;
dev_cba_infection_totalTime = 180;

// Necroplague - Infected
dev_zombie_attack_damageMn = 0.125;
dev_zombie_attack_damageVeh = 0.001;
dev_zombie_attack_launchVeh = "[0,1,1]";
dev_zombie_attack_reachMan = 3.5;
dev_zombie_attack_reachVeh = 8;
dev_zombie_attack_timeout = 0.5;
dev_zombie_distance_agro = 50;
dev_zombie_distance_hunt = 100;
dev_zombie_distance_max = 500;
dev_zombie_distance_roam = 75;
force dev_zombie_greenEyes = true;
dev_zombie_head_caliber = 9;
dev_zombie_head_chance = 0.5;
dev_zombie_health = 0.5;
dev_zombie_infectionChance = 0.5;
force dev_zombie_uniformFix = false;
force dev_zombie_useGlasses = false;
force dev_zombie_useIdentity = true;

// Necroplague - Spitter
dev_toxmut_attack_damageMan = 0.3;
dev_toxmut_attack_damageManSpit = 0.5;
dev_toxmut_attack_damageVeh = 0.01;
dev_toxmut_attack_launchVeh = "[0,1,0.1]";
dev_toxmut_attack_reachMan = 3.5;
dev_toxmut_attack_reachSpit = 30;
dev_toxmut_attack_reachVeh = 8;
dev_toxmut_attack_specialChance = 0.5;
dev_toxmut_attack_timeout = 0.5;
dev_toxmut_attack_timeoutSpit = 15;
dev_toxmut_distance_agro = 75;
dev_toxmut_distance_hunt = 300;
dev_toxmut_distance_max = 1000;
dev_toxmut_distance_roam = 75;
dev_toxmut_health = 1;

// Necroplague - Stalker
dev_form939_attack_damageMan = 0.5;
dev_form939_attack_damageVeh = 0.01;
dev_form939_attack_launchVeh = "[0,1,1]";
dev_form939_attack_psychChance = 0.1;
dev_form939_attack_psychDuration = 15;
dev_form939_attack_psychTimeout = 30;
dev_form939_attack_reachMan = 5;
dev_form939_attack_reachVeh = 8;
dev_form939_attack_timeout = 0.5;
dev_form939_distance_agro = 100;
dev_form939_distance_hunt = 1400;
dev_form939_distance_max = 1500;
dev_form939_distance_roam = 75;
dev_form939_health = 0.11;

// Necroplague - The Bully
dev_asymhuman_attack_bigArmChance = 0.5;
dev_asymhuman_attack_damageMan = 0.5;
dev_asymhuman_attack_damageVeh = 0.2;
dev_asymhuman_attack_infectionChance = 0.5;
dev_asymhuman_attack_launchMan = "[0,6,5]";
dev_asymhuman_attack_launchVeh = "[0,10,5]";
dev_asymhuman_attack_reachMan = 3.5;
dev_asymhuman_attack_reachVeh = 8;
dev_asymhuman_attack_timeout = 0.5;
dev_asymhuman_distance_agro = 75;
dev_asymhuman_distance_hunt = 300;
dev_asymhuman_distance_max = 1000;
dev_asymhuman_distance_roam = 75;
dev_asymhuman_health = 1;

// Necroplague - The Hivemind
dev_hivemind_hallucinate2Distance = 50;
dev_hivemind_hallucinateProbabilityClose = 20;
dev_hivemind_hallucinateProbabilityFar = 20;
dev_hivemind_health = 0.2;
dev_hivemind_maxCooldown = 40;
dev_hivemind_maxDistance = 150;
dev_hivemind_minCooldown = 15;
dev_hivemind_suicideTime = 180;

// Necroplague - The Jumper
dev_asymhuman_stage2_agroDistance = 50;
dev_asymhuman_stage2_attack2Timeout = 10;
dev_asymhuman_stage2_attackDistanceMan = 2.5;
dev_asymhuman_stage2_attackDistanceVeh = 8;
dev_asymhuman_stage2_attackTimeout = 1;
dev_asymhuman_stage2_damageMan = 0.2;
dev_asymhuman_stage2_damageManAttack2 = 0.3;
dev_asymhuman_stage2_damageVeh = 0.1;
dev_asymhuman_stage2_distance_roam = 50;
dev_asymhuman_stage2_health = 0.1;
dev_asymhuman_stage2_huntDistance = 75;
dev_asymhuman_stage2_infectionChance = 0.5;
dev_asymhuman_stage2_jumpTimeout = 30;
dev_asymhuman_stage2_maxDistance = 500;

// Necroplague - The Parasite
dev_parasite_agroDistance = 1000;
dev_parasite_attack2Timeout = 4;
dev_parasite_attack3Timeout = 20;
dev_parasite_attackDistanceMan = 3.5;
dev_parasite_attackDistanceVeh = 7;
dev_parasite_attackTimeout = 1;
dev_parasite_damageMan = 0.2;
dev_parasite_damageManAttack2 = 0.2;
dev_parasite_damageManAttack3 = 0.25;
dev_parasite_damageVeh = 0.001;
dev_parasite_distance_roam = 75;
dev_parasite_health = 0.05;
dev_parasite_huntDistance = 1500;
dev_parasite_infectionChance = 1;
dev_parasite_jumpTimeout = 20;
dev_parasite_maxDistance = 2000;
dev_parasite_specialChance = 0.1;

// OPTRE Powered MJOLNIR
OPTRE_HUD_ENEMY_COLOR = [1,0.2,0.2,1];
OPTRE_HUD_FRIENDLY_COLOR = [0.7,1,1,1];
OPTRE_HUD_GROUP_COLOR = [1,1,1,1];
OPTRE_HUD_HUMAN_ICON = "\OPTRE_Suit_Scripts\textures\OPTRE_MJOLNIR_hudTargetInfantry.paa";
OPTRE_HUD_HUMAN_SIZE = 0.3;
OPTRE_HUD_NEUTRAL_COLOR = [1,1,0,1];
OPTRE_HUD_STATIC_ICON = "\OPTRE_Suit_Scripts\textures\OPTRE_MJOLNIR_hudTargetEmplacement.paa";
OPTRE_HUD_STATIC_SIZE = 0.4;
OPTRE_HUD_VEHICLE_ICON = "\OPTRE_Suit_Scripts\textures\OPTRE_MJOLNIR_hudTargetVehicle.paa";
OPTRE_HUD_VEHICLE_SIZE = 0.6;
force OPTRE_JUMP_SUITS_SETTING = "OPTRE_MJOLNIR_Undersuit,OPTRE_MJOLNIR_Dress_Uniform,OPTRE_FC_MJOLNIR_MKVI_Undersuit,OPTRE_FC_Elite_CombatSkin";
force OPTRE_MJOLNIR_ACTIVATE_AI = true;
force OPTRE_MJOLNIR_ALLOW_LOWLIGHT_SETTING = true;
force OPTRE_MJOLNIR_ALLOW_TARGETING_SETTING = true;
OPTRE_MJOLNIR_BOOTUP_COLOR = [0.694,0.933,0.345,1];
OPTRE_MJOLNIR_CAMSHAKE = true;
OPTRE_MJOLNIR_CHARGE_EFFECT_COLOR = [0.8,1,1,0.2];
OPTRE_MJOLNIR_CHARGE_TEXTURE_COLOR = [0.8,1,1,1];
OPTRE_MJOLNIR_DEPLETED_ALERT_COLOR = [1,0.2,0.2,0.8];
OPTRE_MJOLNIR_DEPLETED_EFFECT_COLOR = [1,0.2,0.2,1];
force OPTRE_MJOLNIR_ENABLE_JUMP = true;
force OPTRE_MJOLNIR_ENABLE_SPARKS_HIT = true;
force OPTRE_MJOLNIR_ENABLE_SPARKS_SHIELD = true;
OPTRE_MJOLNIR_ENERGY_BAR_ACTIVE = true;
OPTRE_MJOLNIR_ENERGY_BAR_COLOR = [0.5,0.9,0.9,0.8];
OPTRE_MJOLNIR_ENERGY_BAR_TEXT_COLOR = [0.7,1,1,0.8];
OPTRE_MJOLNIR_HITEFFECT_COLOR = [1,1,0.2,1];
OPTRE_MJOLNIR_HITEFFECT_DEPLETED_COLOR = [1,0.2,0.2,1];
OPTRE_MJOLNIR_HUD_ACTIVE_INTRO = true;
force OPTRE_MJOLNIR_INCREASED_SPEED = true;
OPTRE_MJOLNIR_INFODISPLAY_COLOR = [0.7,1,1,0.8];
OPTRE_MJOLNIR_INFOTEXT_COLOR = [0.7,1,1,0.8];
force OPTRE_MJOLNIR_JUMP_FORWARD = 3;
force OPTRE_MJOLNIR_JUMP_UP_HIGH = 3;
force OPTRE_MJOLNIR_JUMP_UP_LOW = 5;
OPTRE_MJOLNIR_OVERLAY_COLOR = [0.7,1,1,0.4];
force OPTRE_MJOLNIR_OVERWRITE_CONFIG_SHIELD = false;
force OPTRE_MJOLNIR_PREVENT_FALLDAMAGE = true;
OPTRE_MJOLNIR_RADAR_COLOR = [0.7,1,1,0.8];
force OPTRE_MJOLNIR_RECOIL_MODIFIER = 0.3;
force OPTRE_MJOLNIR_SHIELD_DELAY = 0.1;
force OPTRE_MJOLNIR_SHIELD_ENERGY = 100;
force OPTRE_MJOLNIR_SHIELD_ENERGY_AI = 100;
force OPTRE_MJOLNIR_SHIELD_MODIFIER_1 = 1;
force OPTRE_MJOLNIR_SHIELD_MODIFIER_2 = 1.5;
force OPTRE_MJOLNIR_SHIELD_MODIFIER_3 = 2;
force OPTRE_MJOLNIR_SHIELD_MODIFIER_4 = 2.5;
force OPTRE_MJOLNIR_SHIELD_MODIFIER_5 = 3;
force OPTRE_MJOLNIR_SHIELD_MODIFIER_SUITS_1 = "OPTRE_FC_Elite_Armor_Minor";
force OPTRE_MJOLNIR_SHIELD_MODIFIER_SUITS_2 = "OPTRE_FC_Elite_Armor_Major,OPTRE_FC_Elite_Armor_SpecOps";
force OPTRE_MJOLNIR_SHIELD_MODIFIER_SUITS_3 = "OPTRE_FC_Elite_Armor_Ultra";
force OPTRE_MJOLNIR_SHIELD_MODIFIER_SUITS_4 = "OPTRE_FC_Elite_Armor_Zealot,OPTRE_FC_Elite_Armor_FieldMarshal";
force OPTRE_MJOLNIR_SHIELD_MODIFIER_SUITS_5 = "OPTRE_FC_Elite_Armor_HonorGuard,OPTRE_FC_Elite_Armor_HonorGuard_Ultra";
force OPTRE_MJOLNIR_SHIELD_REGEN = 1;
force OPTRE_MJOLNIR_SHIELD_REGEN_AI = 1;
force OPTRE_MJOLNIR_SHOW_ACTIVATE = true;
OPTRE_MJOLNIR_SHOW_DEACTIVATE = false;
OPTRE_MJOLNIR_SHOW_OUTLINE = true;
OPTRE_MJOLNIR_SHOW_OVERLAY = true;
force OPTRE_MJOLNIR_SPEED_MODIFIER = 1.3;
force OPTRE_MJOLNIR_SUPPRESS_RECOIL = true;
force OPTRE_MJOLNIR_TARGETING_INTERVAL_RANGE_SETTING = 100;
force OPTRE_MJOLNIR_TARGETING_MAX_RANGE_SETTING = 1000;
force OPTRE_MJOLNIR_TARGETING_MIN_RANGE_SETTING = 100;
OPTRE_MJOLNIR_WEAPON_ICON_COLOR = [0.7,1,1,0.8];
force OPTRE_POWERED_HELMETS = "OPTRE_MJOLNIR_Mk4Helmet,OPTRE_MJOLNIR_Mk4Helmet_Blue,OPTRE_MJOLNIR_Mk4Helmet_Red,OPTRE_FC_MJOLNIR_MKV_Helmet,OPTRE_FC_MJOLNIR_MKV_Helmet_Black,OPTRE_FC_MJOLNIR_MKV_Helmet_117,OPTRE_FC_MJOLNIR_MKV_Helmet_Caboose,OPTRE_FC_MJOLNIR_MKV_Helmet_Freeman,OPTRE_FC_MJOLNIR_MKV_Helmet_Church,OPTRE_FC_MJOLNIR_MKV_Helmet_Donut,OPTRE_FC_MJOLNIR_MKV_Helmet_Simmons,OPTRE_FC_MJOLNIR_MKV_Helmet_Night,OPTRE_FC_MJOLNIR_MKV_Helmet_Olive,OPTRE_FC_MJOLNIR_MKV_Helmet_Grif,OPTRE_FC_MJOLNIR_MKV_Helmet_Sarge,OPTRE_FC_MJOLNIR_MKV_Helmet_Tucker,OPTRE_MJOLNIR_MkVBHelmet,OPTRE_MJOLNIR_MkVBHelmet_UA,OPTRE_MJOLNIR_MkVBHelmet_UA_HUL,OPTRE_MJOLNIR_MkVBHelmet_Red,OPTRE_MJOLNIR_MkVBHelmet_Blue,OPTRE_MJOLNIR_MkVBHelmet_Black,OPTRE_MJOLNIR_Commando,OPTRE_MJOLNIR_Commando_HUL3,OPTRE_MJOLNIR_Commando_DefaultV_HUL3,OPTRE_MJOLNIR_Commando_SilverV_HUL3,OPTRE_MJOLNIR_Commando_BlueV_HUL3,OPTRE_MJOLNIR_Commando_BlackV_HUL3,OPTRE_MJOLNIR_Commando_Black_HUL3,OPTRE_MJOLNIR_Commando_Black_DefaultV_HUL3,OPTRE_MJOLNIR_Commando_Black_SilverV_HUL3,OPTRE_MJOLNIR_Commando_Black_BlueV_HUL3,OPTRE_MJOLNIR_Commando_Black_BlackV_HUL3,OPTRE_MJOLNIR_Commando_Blue_HUL3,OPTRE_MJOLNIR_Commando_Blue_DefaultV_HUL3,OPTRE_MJOLNIR_Commando_Blue_SilverV_HUL3,OPTRE_MJOLNIR_Commando_Blue_BlueV_HUL3,OPTRE_MJOLNIR_Commando_Blue_BlackV_HUL3,OPTRE_MJOLNIR_Commando_Red_HUL3,OPTRE_MJOLNIR_Commando_Red_DefaultV_HUL3,OPTRE_MJOLNIR_Commando_Red_SilverV_HUL3,OPTRE_MJOLNIR_Commando_Red_BlueV_HUL3,OPTRE_MJOLNIR_Commando_Red_BlackV_HUL3,OPTRE_MJOLNIR_MkVBHelmet_BLKV,OPTRE_MJOLNIR_MkVBHelmet_BLUV,OPTRE_MJOLNIR_MkVBHelmet_SLVV,OPTRE_MJOLNIR_MkVBHelmet_Black_SLVV,OPTRE_MJOLNIR_Commando_DefaultV,OPTRE_MJOLNIR_Commando_SilverV,OPTRE_MJOLNIR_Commando_BlueV,OPTRE_MJOLNIR_Commando_BlackV,OPTRE_MJOLNIR_Commando_Black,OPTRE_MJOLNIR_Commando_Black_DefaultV,OPTRE_MJOLNIR_Commando_Black_SilverV,OPTRE_MJOLNIR_Commando_Black_BlueV,OPTRE_MJOLNIR_Commando_Black_BlackV,OPTRE_MJOLNIR_Commando_Blue,OPTRE_MJOLNIR_Commando_Blue_DefaultV,OPTRE_MJOLNIR_Commando_Blue_SilverV,OPTRE_MJOLNIR_Commando_Blue_BlueV,OPTRE_MJOLNIR_Commando_Blue_BlackV,OPTRE_MJOLNIR_Commando_Red,OPTRE_MJOLNIR_Commando_Red_DefaultV,OPTRE_MJOLNIR_Commando_Red_SilverV,OPTRE_MJOLNIR_Commando_Red_BlueV,OPTRE_MJOLNIR_Commando_Red_BlackV,OPTRE_MJOLNIR_CQB,OPTRE_MJOLNIR_CQC,OPTRE_MJOLNIR_Pilot,OPTRE_MJOLNIR_Operator,OPTRE_MJOLNIR_EOD,OPTRE_MJOLNIR_ODST,OPTRE_FC_MJOLNIR_Mark_VI_Helmet,OPTRE_FC_MJOLNIR_Mark_VI_Helmet_White,OPTRE_FC_MJOLNIR_Mark_VI_Helmet_Olive,OPTRE_FC_MJOLNIR_Mark_VI_Helmet_Tan,OPTRE_FC_MJOLNIR_Mark_VI_Helmet_Tex,OPTRE_FC_MJOLNIR_Mark_VI_Helmet_Caboose,OPTRE_FC_MJOLNIR_Mark_VI_Helmet_Church,OPTRE_FC_MJOLNIR_Mark_VI_Helmet_Donut,OPTRE_FC_MJOLNIR_Mark_VI_Helmet_Grif,OPTRE_FC_MJOLNIR_Mark_VI_Helmet_Simmons,OPTRE_FC_MJOLNIR_Mark_VI_Helmet_Sarge,OPTRE_FC_MJOLNIR_Mark_VI_Helmet_Kaikaina,OPTRE_FC_MJOLNIR_Mark_VI_Helmet_Lopez,OPTRE_FC_MJOLNIR_Mark_VI_Helmet_Doc,OPTRE_FC_MJOLNIR_Mark_VI_Helmet_North,OPTRE_FC_MJOLNIR_Mark_VI_Helmet_South,OPTRE_FC_MJOLNIR_Mark_VI_Helmet_York,OPTRE_FC_MJOLNIR_Mark_VI_Helmet_Washington,OPTRE_FC_MJOLNIR_Mark_VI_Helmet_Tucker,OPTRE_MJOLNIR_EVAHelmet,OPTRE_MJOLNIR_EVAHelmet_Emily,OPTRE_MJOLNIR_MPHelmet,OPTRE_MJOLNIR_ReconHelmet,OPTRE_FC_Elite_Helmet_FieldMarshal,OPTRE_FC_Elite_Helmet_HonorGuard_Ultra,OPTRE_FC_Elite_Helmet_HonorGuard,OPTRE_FC_Elite_Helmet_Major,OPTRE_FC_Elite_Helmet_Officer,OPTRE_FC_Elite_Helmet_Minor,OPTRE_FC_Elite_Helmet_SpecOps,OPTRE_FC_Elite_Helmet_Ultra,OPTRE_FC_Elite_Helmet_Zealot";
force OPTRE_POWERED_SUITS_SETTING = "OPTRE_MJOLNIR_Mk4Armor,OPTRE_MJOLNIR_Mk4Armor_Blue,OPTRE_MJOLNIR_Mk4Armor_Red,OPTRE_FC_MJOLNIR_MKV_Armor,OPTRE_FC_MJOLNIR_MKV_Armor_Black,OPTRE_FC_MJOLNIR_MKV_Armor_117,OPTRE_FC_MJOLNIR_MKV_Armor_Caboose,OPTRE_FC_MJOLNIR_MKV_Armor_Freeman,OPTRE_FC_MJOLNIR_MKV_Armor_Church,OPTRE_FC_MJOLNIR_MKV_Armor_Donut,OPTRE_FC_MJOLNIR_MKV_Armor_Simmons,OPTRE_FC_MJOLNIR_MKV_Armor_Night,OPTRE_FC_MJOLNIR_MKV_Armor_Olive,OPTRE_FC_MJOLNIR_MKV_Armor_Grif,OPTRE_FC_MJOLNIR_MKV_Armor_Sarge,OPTRE_FC_MJOLNIR_MKV_Armor_Tucker,OPTRE_MJOLNIR_MkVBArmor,OPTRE_FC_MJOLNIR_Mark_VI_Armor,OPTRE_FC_MJOLNIR_Mark_VI_Armor_White,OPTRE_FC_MJOLNIR_Mark_VI_Armor_Olive,OPTRE_FC_MJOLNIR_Mark_VI_Armor_Tan,OPTRE_FC_MJOLNIR_Mark_VI_Armor_Tex,OPTRE_FC_MJOLNIR_Mark_VI_Armor_Caboose,OPTRE_FC_MJOLNIR_Mark_VI_Armor_Church,OPTRE_FC_MJOLNIR_Mark_VI_Armor_Tucker,OPTRE_FC_MJOLNIR_Mark_VI_Armor_Donut,OPTRE_FC_MJOLNIR_Mark_VI_Armor_Grif,OPTRE_FC_MJOLNIR_Mark_VI_Armor_Simmons,OPTRE_FC_MJOLNIR_Mark_VI_Armor_Sarge,OPTRE_FC_MJOLNIR_Mark_VI_Armor_Kaikaina,OPTRE_FC_MJOLNIR_Mark_VI_Armor_Lopez,OPTRE_FC_MJOLNIR_Mark_VI_Armor_Doc,OPTRE_FC_MJOLNIR_Mark_VI_Armor_North,OPTRE_FC_MJOLNIR_Mark_VI_Armor_South,OPTRE_FC_MJOLNIR_Mark_VI_Armor_York,OPTRE_FC_MJOLNIR_Mark_VI_Armor_Washington,OPTRE_FC_Elite_Armor_FieldMarshal,OPTRE_FC_Elite_Armor_HonorGuard_Ultra,OPTRE_FC_Elite_Armor_HonorGuard,OPTRE_FC_Elite_Armor_Major,OPTRE_FC_Elite_Armor_Minor,OPTRE_FC_Elite_Armor_SpecOps,OPTRE_FC_Elite_Armor_Ultra,OPTRE_FC_Elite_Armor_Zealot";
force OPTRE_START_ON_INIT = true;

// OPTRE Settings
OPTRE_AircraftHUD_colour = [0.082,0.408,0.039,1];
OPTRE_Debug_Mode = false;
force OPTRE_Enable_AI_Door_On_Gear_Pelican = true;
force OPTRE_Enable_Humans_To_Detach = false;
OPTRE_Enable_Player_Door_On_Gear_Pelican = true;
force OPTRE_Enable_Supercombustion_dev = true;
force OPTRE_Enable_Turret_Detach = true;
OPTRE_Falcon_PIP_Position = "0,1";
force OPTRE_Hijack_FriendlyFireEnabled = true;
force OPTRE_Hijack_Mode = 2;
force OPTRE_Human_Non_Detachable_Turrets = "";
force OPTRE_Spartan_Non_Detachable_Turrets = "";
force OPTRE_Spartan_Randomize = true;

// RD501
rd501_awacs_UpdateRate = 0.5;
force RD501_CRML_RandomMissileSpawnAllowed = true;
force RD501_CRML_RandomMissileSpawnReloadSpeed = 10;
force RD501_CRML_RandomMissileWeaponClass = "RD501_VLS_Launcher_Weapon";
force rd501_fallheight_enabled = false;
force rd501_fallheight_height = 10;
force rd501_fortify_useAmmo = true;
force rd501_fortify_usePreset = true;
force RD501_LogGlobally = true;
rd501_medical_notification_enabled = true;
rd501_medical_notification_message = "%1isaskingthatyoukindlyholdstill.";
rd501_mfd_colours_custom = [0.5,0.5,0.5];
rd501_mfd_colours_custom_alpha = 1;
RD501_MinimumLogLevel = 3;
RD501_ZL_Enabled = true;

// RD501 Radio Dialog Selector
RD501_Radio_Dialog_Selector_Dialog = "anarc210_radio_dialog";
RD501_Radio_Dialog_Selector_Enabled = false;

// SFA Boosts
force force sfa_boosts_akolto_bandageCount = 4;
force force sfa_boosts_akolto_painFactor = 0.1;
force force sfa_boosts_akolto_tickCount = 8;
force force sfa_boosts_akolto_tickDelay = 5;
force force sfa_boosts_ekolto_healWounds = false;
force force sfa_boosts_ekolto_regenerateBlood = false;
force force sfa_boosts_ekolto_removeMedications = false;
force force sfa_boosts_reflex_reducedSwayFactor = 0.25;
force force sfa_boosts_reflex_reducedSwayTime = 300;

// TFAR - Clientside settings
TFAR_curatorCamEars = false;
TFAR_default_radioVolume = 6;
TFAR_intercomDucking = 0.2;
TFAR_intercomVolume = 0.1;
TFAR_moveWhileTabbedOut = false;
TFAR_noAutomoveSpectator = false;
TFAR_oldVolumeHint = false;
TFAR_pluginTimeout = 4;
TFAR_PosUpdateMode = 0.1;
TFAR_showChannelChangedHint = true;
TFAR_ShowDiaryRecord = true;
TFAR_showTransmittingHint = true;
TFAR_ShowVolumeHUD = false;
TFAR_splendidCamEars = false;
TFAR_tangentReleaseDelay = 0;
TFAR_VolumeHudTransparency = 0;
TFAR_volumeModifier_forceSpeech = false;

// TFAR - Global settings
force TFAR_AICanHearPlayer = false;
force TFAR_AICanHearSpeaker = false;
force TFAR_allowDebugging = true;
tfar_core_noTSNotConnectedHint = false;
force TFAR_defaultIntercomSlot = 0;
force TFAR_disableAutoMute = false;
force TFAR_enableIntercom = true;
force TFAR_experimentalVehicleIsolation = true;
force TFAR_externalIntercomEnable = 0;
force TFAR_externalIntercomMaxRange_Phone = 5;
force TFAR_externalIntercomMaxRange_Wireless = 15;
force TFAR_fullDuplex = true;
force TFAR_giveLongRangeRadioToGroupLeaders = false;
force TFAR_giveMicroDagrToSoldier = true;
force TFAR_givePersonalRadioToRegularSoldier = false;
force TFAR_globalRadioRangeCoef = 1;
force TFAR_instantiate_instantiateAtBriefing = false;
force TFAR_objectInterceptionEnabled = false;
force TFAR_objectInterceptionStrength = 0;
force tfar_radiocode_east = "_opfor";
force tfar_radiocode_independent = "_independent";
force tfar_radiocode_west = "_bluefor";
force tfar_radioCodesDisabled = false;
force TFAR_SameLRFrequenciesForSide = false;
force TFAR_SameSRFrequenciesForSide = false;
force TFAR_setting_defaultFrequencies_lr_east = "";
force TFAR_setting_defaultFrequencies_lr_independent = "";
force TFAR_setting_defaultFrequencies_lr_west = "60";
force TFAR_setting_defaultFrequencies_sr_east = "";
force TFAR_setting_defaultFrequencies_sr_independent = "";
force TFAR_setting_defaultFrequencies_sr_west = "123";
force TFAR_setting_DefaultRadio_Airborne_east = "TFAR_mr6000l";
force TFAR_setting_DefaultRadio_Airborne_Independent = "TFAR_anarc164";
force TFAR_setting_DefaultRadio_Airborne_West = "TFAR_anarc210";
force TFAR_setting_DefaultRadio_Backpack_east = "TFAR_mr3000";
force TFAR_setting_DefaultRadio_Backpack_Independent = "TFAR_anprc155";
force TFAR_setting_DefaultRadio_Backpack_west = "TFAR_rt1523g";
force TFAR_setting_DefaultRadio_Personal_east = "TFAR_fadak";
force TFAR_setting_DefaultRadio_Personal_Independent = "TFAR_anprc148jem";
force TFAR_setting_DefaultRadio_Personal_West = "TFAR_anprc152";
force TFAR_setting_DefaultRadio_Rifleman_East = "TFAR_pnr1000a";
force TFAR_setting_DefaultRadio_Rifleman_Independent = "TFAR_anprc154";
force TFAR_setting_DefaultRadio_Rifleman_West = "TFAR_rf7800str";
force TFAR_setting_externalIntercomWirelessHeadgear = "";
force TFAR_spectatorCanHearEnemyUnits = true;
force TFAR_spectatorCanHearFriendlies = true;
force TFAR_takingRadio = 2;
force TFAR_Teamspeak_Channel_Password = "123";
force tfar_terrain_interception_coefficient = 0;
force TFAR_voiceCone = true;

// Zeus Enhanced
force zen_area_markers_editableMarkers = 0;
zen_building_markers_enabled = false;
zen_camera_adaptiveSpeed = true;
zen_camera_defaultSpeedCoef = 1;
zen_camera_fastSpeedCoef = 1;
zen_camera_followTerrain = true;
force force zen_common_ascensionMessages = true;
force zen_common_autoAddObjects = true;
force zen_common_cameraBird = false;
zen_common_darkMode = false;
zen_common_disableGearAnim = false;
zen_common_preferredArsenal = 1;
zen_compat_ace_hideModules = true;
zen_context_menu_enabled = 2;
zen_context_menu_overrideWaypoints = false;
zen_editor_addGroupIcons = false;
zen_editor_addModIcons = false;
zen_editor_declutterEmptyTree = true;
zen_editor_disableLiveSearch = false;
zen_editor_moveDisplayToEdge = true;
force zen_editor_parachuteSounds = true;
zen_editor_previews_enabled = true;
zen_editor_randomizeCopyPaste = false;
zen_editor_removeWatermark = true;
zen_editor_unitRadioMessages = 0;
zen_placement_enabled = false;
zen_remote_control_cameraExitPosition = 2;
zen_visibility_enabled = 0;
zen_visibility_maxDistance = 5000;
zen_vision_enableBlackHot = false;
zen_vision_enableBlackHotGreenCold = false;
zen_vision_enableBlackHotRedCold = false;
zen_vision_enableGreenHotCold = false;
zen_vision_enableNVG = true;
zen_vision_enableRedGreenThermal = false;
zen_vision_enableRedHotCold = false;
zen_vision_enableWhiteHot = true;
zen_vision_enableWhiteHotRedCold = false;

// Zeus Enhanced - Attributes
zen_attributes_enableAbilities = true;
zen_attributes_enableAmmo = true;
zen_attributes_enableAmmoCargo = true;
zen_attributes_enableArsenal = true;
zen_attributes_enableBuildingMarker = true;
zen_attributes_enableDamage = true;
zen_attributes_enableEngine = true;
zen_attributes_enableExecute = true;
zen_attributes_enableFuel = true;
zen_attributes_enableFuelCargo = true;
zen_attributes_enableGarage = true;
zen_attributes_enableGroupBehaviour = true;
zen_attributes_enableGroupCombatMode = true;
zen_attributes_enableGroupExecute = true;
zen_attributes_enableGroupFormation = true;
zen_attributes_enableGroupID = true;
zen_attributes_enableGroupSide = true;
zen_attributes_enableGroupSkill = true;
zen_attributes_enableGroupSpeed = true;
zen_attributes_enableGroupStance = true;
zen_attributes_enableHealth = true;
zen_attributes_enableInventory = true;
zen_attributes_enableLights = true;
zen_attributes_enableMarkerAlpha = true;
zen_attributes_enableMarkerColor = true;
zen_attributes_enableMarkerText = true;
zen_attributes_enableName = true;
zen_attributes_enablePlateNumber = true;
zen_attributes_enableRank = true;
zen_attributes_enableRepairCargo = true;
zen_attributes_enableRespawnPosition = true;
zen_attributes_enableRespawnVehicle = true;
zen_attributes_enableSensors = true;
zen_attributes_enableSkill = true;
zen_attributes_enableSkills = true;
zen_attributes_enableStance = true;
zen_attributes_enableStates = true;
zen_attributes_enableTraits = true;
zen_attributes_enableVehicleLock = true;
zen_attributes_enableWaypointBehaviour = true;
zen_attributes_enableWaypointCombatMode = true;
zen_attributes_enableWaypointFormation = true;
zen_attributes_enableWaypointLoiterAltitude = true;
zen_attributes_enableWaypointLoiterDirection = true;
zen_attributes_enableWaypointLoiterRadius = true;
zen_attributes_enableWaypointSpeed = true;
zen_attributes_enableWaypointTimeout = true;
zen_attributes_enableWaypointType = true;

// Zeus Enhanced - Faction Filter
zen_faction_filter_0_ = true;
zen_faction_filter_0_3AS_CIS = true;
zen_faction_filter_0_3AS_Rebel = true;
zen_faction_filter_0_442_CIS = true;
zen_faction_filter_0_Aux501_Editor_Category_Confederacy = true;
zen_faction_filter_0_Aux501_Editor_Category_Mandalorians = true;
zen_faction_filter_0_dev_groups_east = true;
zen_faction_filter_0_dev_mutants = true;
zen_faction_filter_0_IBL_faction_Empire = true;
zen_faction_filter_0_JLTS_CIS = true;
zen_faction_filter_0_LS_CIS = true;
zen_faction_filter_0_LS_PIRATES = true;
zen_faction_filter_0_O_DBA_CIS_F = true;
zen_faction_filter_0_OPF_F = true;
zen_faction_filter_0_OPF_G_F = true;
zen_faction_filter_0_OPF_GEN_F = true;
zen_faction_filter_0_OPF_R_F = true;
zen_faction_filter_0_OPF_T_F = true;
zen_faction_filter_0_OPTRE_Ins = true;
zen_faction_filter_0_OPTRE_Ins_groups = true;
zen_faction_filter_0_RD501_CIS_Faction = true;
zen_faction_filter_0_SFA_groups_imp = true;
zen_faction_filter_0_SFA_Sith_Assets = true;
zen_faction_filter_0_WM_ParaRebel = true;
zen_faction_filter_0_WM_ParaRebel_groups = true;
zen_faction_filter_0_WM_Rebels = true;
zen_faction_filter_1_3AS_Imperial = true;
zen_faction_filter_1_3AS_Rep = true;
zen_faction_filter_1_44_ab = true;
zen_faction_filter_1_Aux501_Editor_Category_Republic = true;
zen_faction_filter_1_B_DBA_Neutral_F_Faction = true;
zen_faction_filter_1_BLU_CTRG_F = true;
zen_faction_filter_1_BLU_F = true;
zen_faction_filter_1_BLU_G_F = true;
zen_faction_filter_1_BLU_GEN_F = true;
zen_faction_filter_1_BLU_T_F = true;
zen_faction_filter_1_BLU_W_F = true;
zen_faction_filter_1_dev_groups_west = true;
zen_faction_filter_1_dev_mutants = true;
zen_faction_filter_1_JLTS_GAR = true;
zen_faction_filter_1_kobra_b = true;
zen_faction_filter_1_LS_CSF = true;
zen_faction_filter_1_LS_GAR = true;
zen_faction_filter_1_LS_ORSF = true;
zen_faction_filter_1_LS_REPNAVY = true;
zen_faction_filter_1_lsb_turret = true;
zen_faction_filter_1_OPTRE_UNSC = true;
zen_faction_filter_1_SFA_groups_rep = true;
zen_faction_filter_1_SFA_Rep_Assets = true;
zen_faction_filter_1_SWLB_GAR = true;
zen_faction_filter_1_SWLB_GAR_SOB = true;
zen_faction_filter_1_WM_Empire = true;
zen_faction_filter_2_ = true;
zen_faction_filter_2_3AS_BlackSun = true;
zen_faction_filter_2_Aux501_Editor_Category_PDFs = true;
zen_faction_filter_2_Aux501_FactionClasses_PDF = true;
zen_faction_filter_2_dev_groups_indep = true;
zen_faction_filter_2_dev_mutants = true;
zen_faction_filter_2_IND_C_F = true;
zen_faction_filter_2_IND_E_F = true;
zen_faction_filter_2_IND_F = true;
zen_faction_filter_2_IND_G_F = true;
zen_faction_filter_2_IND_L_F = true;
zen_faction_filter_2_LS_MANDALORIAN = true;
zen_faction_filter_2_LS_MEMEFOR = true;
zen_faction_filter_2_LS_MERC = true;
zen_faction_filter_2_OPF_F = true;
zen_faction_filter_2_OPTRE_CAA = true;
zen_faction_filter_2_OPTRE_DME = true;
zen_faction_filter_2_OPTRE_Ins = true;
zen_faction_filter_2_OPTRE_PD = true;
zen_faction_filter_2_OPTRE_UEG_Civ = true;
zen_faction_filter_2_SFA_Mando_Assets = true;
zen_faction_filter_2_SFA_Revanite_Assets = true;
zen_faction_filter_3_3AS_Civilian = true;
zen_faction_filter_3_Aux501_Editor_Category_Civilians = true;
zen_faction_filter_3_Aux501_Editor_Category_Republic = true;
zen_faction_filter_3_CIV_F = true;
zen_faction_filter_3_CIV_IDAP_F = true;
zen_faction_filter_3_dev_mutants = true;
zen_faction_filter_3_EdCat_jbad_vehicles = true;
zen_faction_filter_3_IND_L_F = true;
zen_faction_filter_3_LS_CIV = true;
zen_faction_filter_3_OPTRE_UEG_Civ = true;


// 501st TFAR Channel Setting, Automatically Generated
force force TFAR_Teamspeak_Channel_Name = "TFR:13";
